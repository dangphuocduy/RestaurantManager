﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using Janus.Windows.GridEX;

namespace RestaurantManager
{
    public partial class InvoicesForm : BaseForm
    {
        public HoaDon HoaDon;
        public decimal Total;
        public InvoicesForm()
        {
            InitializeComponent();
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    decimal SoLuong = (decimal)e.Row.Cells["SoLuong"].Value;
                    decimal ThanhTien = (decimal)e.Row.Cells["ThanhTien"].Value;
                    decimal DonGia = (decimal)e.Row.Cells["DonGia"].Value;

                    e.Row.Cells["SoLuong"].Text = ToTrimmedString(SoLuong);
                    e.Row.Cells["ThanhTien"].Text = ThanhTien.ToString("#,##0");
                    e.Row.Cells["DonGia"].Text = DonGia.ToString("#,##0");
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public string ToTrimmedString(decimal num)
        {
            try
            {
                string str = num.ToString();
                string decimalSeparator = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
                if (str.Contains(decimalSeparator))
                {
                    str = str.TrimEnd('0');
                    if (str.EndsWith(decimalSeparator))
                    {
                        str = str.Remove(str.Length - 1, 1);
                    }
                }
                return str;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return null;
        }

        public string RemoveFromEnd(string str, int characterCount)
        {
            try
            {
                return str.Remove(str.Length - characterCount, characterCount);

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        private void InvoicesForm_Load(object sender, EventArgs e)
        {
            try
            {
                if (HoaDon != null)
                {
                    lblTableName.Text = HoaDon.PhongBan;
                    lblThoiGianTao.Text = HoaDon.ThoiGianThanhToan.ToString("dd-MM-yyyy HH:mm");
                    CultureInfo cultureInfo = CultureInfo.GetCultureInfo("vi-VN");
                    string TongTien = decimal.Parse(HoaDon.TongTien.ToString()).ToString("#,###", cultureInfo.NumberFormat);
                    if (HoaDon.ThueSuat != null)
                    {
                        if (HoaDon.ThueSuat != 0)
                        {                            
                            foreach (var item in HoaDon.HangHoaCollection)
                            {
                                Total += item.ThanhTien;
                            }
                            string Tax = decimal.Parse((Total * HoaDon.ThueSuat / 100).ToString()).ToString("#,###", cultureInfo.NumberFormat);
                            string ToalTax = decimal.Parse(Total.ToString()).ToString("#,###", cultureInfo.NumberFormat);
                            lblTax.Text = Tax;
                            lblToTalTax.Text = ToalTax;
                            lblTongTien.Text = TongTien;
                        }
                        else
                        {
                            lblTongTien.Text = TongTien;
                            lblTax.Text = HoaDon.ThueSuat.ToString();
                            lblToTalTax.Text = TongTien;
                        }
                    }
                    else
                    {
                        lblTongTien.Text = TongTien;
                        lblToTalTax.Text = TongTien;
                    }

                    dgList.Refetch();
                    dgList.DataSource = HoaDon.HangHoaCollection;
                    dgList.Refresh();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                if (HoaDon != null)
                {
                    if (HoaDon.HangHoaCollection.Count() > 0)
                    {
                        K80PrintTemplates f = new K80PrintTemplates();
                        f.HoaDon = HoaDon;
                        f.BindReport();
                        if (GlobalSettings.PREVIEW)
                        {
                            f.ShowPreviewDialog();
                        }
                        else
                        {
                            f.Print();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }
    }
}
