using System;
using System.Data;
using System.Text;
using System.Collections;
using System.Security.Cryptography;
using RestaurantManager;

namespace ManagementPrintFromExcel
{
    class SiteIdentity : System.Security.Principal.IIdentity
    {
        public User user;
        public long User_ID;
        public string AuthenticationType
        {
            get
            {
                return "Custom Authentication";
            }
            set
            {
                // Do Nothing
            }
        }
        public bool IsAuthenticated
        {
            get
            {
                return true;
            }
        }
        public string UserName
        {
            get
            {
                return user.UserName;
            }
        }
        public string FullName
        {
            get
            {
                return user.FullName;
            }
        }
        //public SiteIdentity(string currentUserName)
        //{
        //    user = new User();
        //    if (!user.Load(currentUserName))
        //        user = null;
        //}
        public SiteIdentity(long currentUserID)
        {
            User_ID = currentUserID;
            user = User.Load(User_ID);
            if (user == null)
            {
                User_ID = 0;
            }
        }
        //public bool TestPassword(string password)
        //{
        //    // At some point, we may have a more complex way of encrypting or storing the passwords
        //    // so by supplying this procedure, we can simply replace its contents to move password
        //    // comparison to the database (as we've done below) or somewhere else (e.g. another
        //    // web service, etc).

        //    UnicodeEncoding encoding = new UnicodeEncoding();
        //    byte[] hashBytes = encoding.GetBytes(password);

        //    SHA1 sha1 = new SHA1CryptoServiceProvider();
        //    byte[] bytePassword = sha1.ComputeHash(hashBytes);
        //    string cryptPassword = Convert.ToBase64String(bytePassword);

        //    return user.TestPassWord(user.ID, cryptPassword);
        //}

        #region IIdentity Members


        public string Name
        {
            get { throw new NotImplementedException(); }
        }

        #endregion

        #region IIdentity Members

        string System.Security.Principal.IIdentity.AuthenticationType
        {
            get { throw new NotImplementedException(); }
        }

        bool System.Security.Principal.IIdentity.IsAuthenticated
        {
            get { throw new NotImplementedException(); }
        }

        string System.Security.Principal.IIdentity.Name
        {
            get { throw new NotImplementedException(); }
        }

        #endregion
    }
}
