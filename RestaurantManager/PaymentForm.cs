﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

namespace RestaurantManager
{
    public partial class PaymentForm : BaseForm
    {
        public Payment Payment;
        public PaymentForm()
        {
            InitializeComponent();
        }

        private void PaymentForm_Load(object sender, EventArgs e)
        {
            LoadCategoty();
            if (Payment != null)
            {
                txtThoiGian.Value = Payment.ThoiGian;
                txtNguoiNhan.Text = Payment.NguoiNhan;
                cbbLoaiChi.SelectedValue = Payment.PaymentTypeId;
                txtGhiChu.Text = Payment.GhiChu;
                txtGiaTri.Text = Payment.GiaTri.ToString();
            }
            else
            {
                Payment = new Payment();
            }
        }
        private void LoadCategoty()
        {
            try
            {
                cbbLoaiChi.DataSource = PaymentType.SelectAll().Tables[0];
                cbbLoaiChi.DisplayMember = "Ten";
                cbbLoaiChi.ValueMember = "Id";
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void GetData()
        {
            try
            {
                TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
                Payment.ThoiGian = txtThoiGian.Value;
                Payment.NguoiNhan = textInfo.ToTitleCase(txtNguoiNhan.Text.Trim().ToLower());
                Payment.PaymentTypeId = Convert.ToInt64(cbbLoaiChi.SelectedValue);
                Payment.GhiChu = textInfo.ToTitleCase(txtGhiChu.Text.Trim().ToLower());
                Payment.GiaTri = Convert.ToDecimal(txtGiaTri.Text.Replace(" ₫", ""));
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            isValid &= ValidateControl.ValidateDate(txtThoiGian, errorProvider, "Thời gian", isOnlyWarning);
            isValid &= ValidateControl.ValidateChoose(cbbLoaiChi, errorProvider, "Loại chi", isOnlyWarning);
            isValid &= ValidateControl.ValidateNull(txtGiaTri, errorProvider, "Giá trị", isOnlyWarning);
            return isValid;
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;

                GetData();
                Payment.InsertUpdate();
                ShowMessage("Lưu thông tin thành công", false, false);
                Helpers Helpers = new Helpers();
                Helpers.SendEmmailPayment(Payment);
                this.Close();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnSaveAndNew_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;

                GetData();
                Payment.InsertUpdate();
                ShowMessage("Lưu thông tin thành công", false, false);
                Payment = new Payment();
                txtNguoiNhan.Text = String.Empty;
                txtGhiChu.Text = String.Empty;
                txtGiaTri.Text = String.Empty;
                Helpers Helpers = new Helpers();
                Helpers.SendEmmailPayment(Payment);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
