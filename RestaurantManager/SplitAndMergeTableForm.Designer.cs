﻿namespace RestaurantManager
{
    partial class SplitAndMergeTableForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SplitAndMergeTableForm));
            Janus.Windows.GridEX.GridEXLayout dgListMerge_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dglistDestination_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListSource_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.rdSplit = new Janus.Windows.EditControls.UIRadioButton();
            this.rdMerge = new Janus.Windows.EditControls.UIRadioButton();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnSave = new Janus.Windows.EditControls.UIButton();
            this.btnCancel = new Janus.Windows.EditControls.UIButton();
            this.tabMain = new Janus.Windows.UI.Tab.UITab();
            this.tabMerge = new Janus.Windows.UI.Tab.UITabPage();
            this.dgListMerge = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.cbbTableMerge = new Janus.Windows.EditControls.UIComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tabSplit = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox7 = new Janus.Windows.EditControls.UIGroupBox();
            this.dglistDestination = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnBackTo = new Janus.Windows.EditControls.UIButton();
            this.btnTransfer = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox8 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgListSource = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtSoLuong = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label12 = new System.Windows.Forms.Label();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.cbbTableSplit = new Janus.Windows.EditControls.UIComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.errorProvider = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabMain)).BeginInit();
            this.tabMain.SuspendLayout();
            this.tabMerge.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListMerge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            this.tabSplit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).BeginInit();
            this.uiGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dglistDestination)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).BeginInit();
            this.uiGroupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.Controls.Add(this.rdSplit);
            this.uiGroupBox1.Controls.Add(this.rdMerge);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(986, 61);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // rdSplit
            // 
            this.rdSplit.Location = new System.Drawing.Point(191, 20);
            this.rdSplit.Name = "rdSplit";
            this.rdSplit.Size = new System.Drawing.Size(161, 23);
            this.rdSplit.TabIndex = 1;
            this.rdSplit.Text = "Tách đơn";
            this.rdSplit.CheckedChanged += new System.EventHandler(this.rdSplit_CheckedChanged);
            // 
            // rdMerge
            // 
            this.rdMerge.Location = new System.Drawing.Point(24, 20);
            this.rdMerge.Name = "rdMerge";
            this.rdMerge.Size = new System.Drawing.Size(161, 23);
            this.rdMerge.TabIndex = 0;
            this.rdMerge.Text = "Ghép đơn";
            this.rdMerge.CheckedChanged += new System.EventHandler(this.rdMerge_CheckedChanged);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.AutoScroll = true;
            this.uiGroupBox2.Controls.Add(this.btnSave);
            this.uiGroupBox2.Controls.Add(this.btnCancel);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 494);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(986, 51);
            this.uiGroupBox2.TabIndex = 2;
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageSize = new System.Drawing.Size(20, 20);
            this.btnSave.Location = new System.Drawing.Point(733, 10);
            this.btnSave.Margin = new System.Windows.Forms.Padding(6);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(113, 35);
            this.btnSave.TabIndex = 10;
            this.btnSave.Text = "Thực hiện";
            this.btnSave.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.Image = ((System.Drawing.Image)(resources.GetObject("btnCancel.Image")));
            this.btnCancel.ImageSize = new System.Drawing.Size(20, 20);
            this.btnCancel.Location = new System.Drawing.Point(858, 11);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(6);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(113, 35);
            this.btnCancel.TabIndex = 11;
            this.btnCancel.Text = "Bỏ qua";
            this.btnCancel.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // tabMain
            // 
            this.tabMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabMain.Location = new System.Drawing.Point(0, 61);
            this.tabMain.Name = "tabMain";
            this.tabMain.Size = new System.Drawing.Size(986, 433);
            this.tabMain.TabIndex = 3;
            this.tabMain.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.tabMerge,
            this.tabSplit});
            this.tabMain.VisualStyle = Janus.Windows.UI.Tab.TabVisualStyle.Office2003;
            // 
            // tabMerge
            // 
            this.tabMerge.Controls.Add(this.dgListMerge);
            this.tabMerge.Controls.Add(this.uiGroupBox3);
            this.tabMerge.Image = ((System.Drawing.Image)(resources.GetObject("tabMerge.Image")));
            this.tabMerge.Location = new System.Drawing.Point(1, 32);
            this.tabMerge.Name = "tabMerge";
            this.tabMerge.Size = new System.Drawing.Size(984, 400);
            this.tabMerge.TabStop = true;
            this.tabMerge.Text = "Ghép đơn";
            // 
            // dgListMerge
            // 
            this.dgListMerge.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListMerge.ColumnAutoResize = true;
            dgListMerge_DesignTimeLayout.LayoutString = resources.GetString("dgListMerge_DesignTimeLayout.LayoutString");
            this.dgListMerge.DesignTimeLayout = dgListMerge_DesignTimeLayout;
            this.dgListMerge.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListMerge.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgListMerge.GroupByBoxVisible = false;
            this.dgListMerge.Location = new System.Drawing.Point(0, 62);
            this.dgListMerge.Margin = new System.Windows.Forms.Padding(6);
            this.dgListMerge.Name = "dgListMerge";
            this.dgListMerge.RecordNavigator = true;
            this.dgListMerge.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListMerge.Size = new System.Drawing.Size(984, 338);
            this.dgListMerge.TabIndex = 13;
            this.dgListMerge.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgListMerge.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgListMerge_LoadingRow);
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.AutoScroll = true;
            this.uiGroupBox3.Controls.Add(this.cbbTableMerge);
            this.uiGroupBox3.Controls.Add(this.label4);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(984, 62);
            this.uiGroupBox3.TabIndex = 1;
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // cbbTableMerge
            // 
            this.cbbTableMerge.Location = new System.Drawing.Point(135, 19);
            this.cbbTableMerge.Name = "cbbTableMerge";
            this.cbbTableMerge.Size = new System.Drawing.Size(204, 29);
            this.cbbTableMerge.TabIndex = 32;
            this.cbbTableMerge.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(19, 24);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 20);
            this.label4.TabIndex = 33;
            this.label4.Text = "Ghép đến : ";
            // 
            // tabSplit
            // 
            this.tabSplit.Controls.Add(this.uiGroupBox7);
            this.tabSplit.Controls.Add(this.uiGroupBox6);
            this.tabSplit.Controls.Add(this.uiGroupBox8);
            this.tabSplit.Controls.Add(this.uiGroupBox5);
            this.tabSplit.Controls.Add(this.uiGroupBox4);
            this.tabSplit.Image = ((System.Drawing.Image)(resources.GetObject("tabSplit.Image")));
            this.tabSplit.Location = new System.Drawing.Point(1, 32);
            this.tabSplit.Name = "tabSplit";
            this.tabSplit.Size = new System.Drawing.Size(984, 400);
            this.tabSplit.TabStop = true;
            this.tabSplit.Text = "Tách đơn";
            // 
            // uiGroupBox7
            // 
            this.uiGroupBox7.Controls.Add(this.dglistDestination);
            this.uiGroupBox7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox7.Location = new System.Drawing.Point(580, 122);
            this.uiGroupBox7.Name = "uiGroupBox7";
            this.uiGroupBox7.Size = new System.Drawing.Size(404, 278);
            this.uiGroupBox7.TabIndex = 7;
            this.uiGroupBox7.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // dglistDestination
            // 
            this.dglistDestination.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dglistDestination.ColumnAutoResize = true;
            dglistDestination_DesignTimeLayout.LayoutString = resources.GetString("dglistDestination_DesignTimeLayout.LayoutString");
            this.dglistDestination.DesignTimeLayout = dglistDestination_DesignTimeLayout;
            this.dglistDestination.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dglistDestination.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dglistDestination.GroupByBoxVisible = false;
            this.dglistDestination.Location = new System.Drawing.Point(3, 8);
            this.dglistDestination.Margin = new System.Windows.Forms.Padding(6);
            this.dglistDestination.Name = "dglistDestination";
            this.dglistDestination.RecordNavigator = true;
            this.dglistDestination.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dglistDestination.Size = new System.Drawing.Size(398, 267);
            this.dglistDestination.TabIndex = 15;
            this.dglistDestination.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dglistDestination.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dglistDestination_LoadingRow);
            this.dglistDestination.Click += new System.EventHandler(this.dglistDestination_Click);
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.Controls.Add(this.btnBackTo);
            this.uiGroupBox6.Controls.Add(this.btnTransfer);
            this.uiGroupBox6.Dock = System.Windows.Forms.DockStyle.Left;
            this.uiGroupBox6.Location = new System.Drawing.Point(441, 122);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(139, 278);
            this.uiGroupBox6.TabIndex = 6;
            this.uiGroupBox6.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnBackTo
            // 
            this.btnBackTo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBackTo.Image = ((System.Drawing.Image)(resources.GetObject("btnBackTo.Image")));
            this.btnBackTo.ImageSize = new System.Drawing.Size(20, 20);
            this.btnBackTo.Location = new System.Drawing.Point(5, 128);
            this.btnBackTo.Margin = new System.Windows.Forms.Padding(6);
            this.btnBackTo.Name = "btnBackTo";
            this.btnBackTo.Size = new System.Drawing.Size(125, 35);
            this.btnBackTo.TabIndex = 42;
            this.btnBackTo.Text = "Chuyển lại";
            this.btnBackTo.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnBackTo.Click += new System.EventHandler(this.btnBackTo_Click);
            // 
            // btnTransfer
            // 
            this.btnTransfer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTransfer.Image = ((System.Drawing.Image)(resources.GetObject("btnTransfer.Image")));
            this.btnTransfer.ImageSize = new System.Drawing.Size(20, 20);
            this.btnTransfer.Location = new System.Drawing.Point(5, 81);
            this.btnTransfer.Margin = new System.Windows.Forms.Padding(6);
            this.btnTransfer.Name = "btnTransfer";
            this.btnTransfer.Size = new System.Drawing.Size(125, 35);
            this.btnTransfer.TabIndex = 41;
            this.btnTransfer.Text = "Chuyển qua";
            this.btnTransfer.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnTransfer.Click += new System.EventHandler(this.btnTransfer_Click);
            // 
            // uiGroupBox8
            // 
            this.uiGroupBox8.Controls.Add(this.dgListSource);
            this.uiGroupBox8.Dock = System.Windows.Forms.DockStyle.Left;
            this.uiGroupBox8.Location = new System.Drawing.Point(0, 122);
            this.uiGroupBox8.Name = "uiGroupBox8";
            this.uiGroupBox8.Size = new System.Drawing.Size(441, 278);
            this.uiGroupBox8.TabIndex = 4;
            this.uiGroupBox8.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // dgListSource
            // 
            this.dgListSource.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListSource.ColumnAutoResize = true;
            dgListSource_DesignTimeLayout.LayoutString = resources.GetString("dgListSource_DesignTimeLayout.LayoutString");
            this.dgListSource.DesignTimeLayout = dgListSource_DesignTimeLayout;
            this.dgListSource.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListSource.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgListSource.GroupByBoxVisible = false;
            this.dgListSource.Location = new System.Drawing.Point(3, 8);
            this.dgListSource.Margin = new System.Windows.Forms.Padding(6);
            this.dgListSource.Name = "dgListSource";
            this.dgListSource.RecordNavigator = true;
            this.dgListSource.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListSource.Size = new System.Drawing.Size(435, 267);
            this.dgListSource.TabIndex = 14;
            this.dgListSource.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgListSource.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgListSource_LoadingRow);
            this.dgListSource.Click += new System.EventHandler(this.dgListSource_Click);
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.Controls.Add(this.txtSoLuong);
            this.uiGroupBox5.Controls.Add(this.label12);
            this.uiGroupBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox5.Location = new System.Drawing.Point(0, 66);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(984, 56);
            this.uiGroupBox5.TabIndex = 3;
            this.uiGroupBox5.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // txtSoLuong
            // 
            this.txtSoLuong.Location = new System.Drawing.Point(441, 15);
            this.txtSoLuong.Name = "txtSoLuong";
            this.txtSoLuong.Size = new System.Drawing.Size(139, 29);
            this.txtSoLuong.TabIndex = 37;
            this.txtSoLuong.Text = "0,00";
            this.txtSoLuong.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.txtSoLuong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoLuong.TextChanged += new System.EventHandler(this.txtSoLuong_TextChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(328, 19);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(84, 20);
            this.label12.TabIndex = 39;
            this.label12.Text = "Số lượng : ";
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.AutoScroll = true;
            this.uiGroupBox4.Controls.Add(this.cbbTableSplit);
            this.uiGroupBox4.Controls.Add(this.label1);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox4.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(984, 66);
            this.uiGroupBox4.TabIndex = 2;
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // cbbTableSplit
            // 
            this.cbbTableSplit.Location = new System.Drawing.Point(135, 19);
            this.cbbTableSplit.Name = "cbbTableSplit";
            this.cbbTableSplit.Size = new System.Drawing.Size(204, 29);
            this.cbbTableSplit.TabIndex = 34;
            this.cbbTableSplit.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbbTableSplit.SelectedValueChanged += new System.EventHandler(this.cbbTableSplit_SelectedValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(19, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 20);
            this.label1.TabIndex = 35;
            this.label1.Text = "Tách đến : ";
            // 
            // errorProvider
            // 
            this.errorProvider.ContainerControl = this;
            this.errorProvider.Icon = ((System.Drawing.Icon)(resources.GetObject("errorProvider.Icon")));
            // 
            // SplitAndMergeTableForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(986, 545);
            this.Controls.Add(this.tabMain);
            this.Controls.Add(this.uiGroupBox2);
            this.Controls.Add(this.uiGroupBox1);
            this.Name = "SplitAndMergeTableForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Tách ghép bàn";
            this.Load += new System.EventHandler(this.SplitAndMergeTableForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabMain)).EndInit();
            this.tabMain.ResumeLayout(false);
            this.tabMerge.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListMerge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            this.tabSplit.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).EndInit();
            this.uiGroupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dglistDestination)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox8)).EndInit();
            this.uiGroupBox8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.uiGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.UI.Tab.UITab tabMain;
        private Janus.Windows.UI.Tab.UITabPage tabMerge;
        private Janus.Windows.UI.Tab.UITabPage tabSplit;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.GridEX.GridEX dgListMerge;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox7;
        private Janus.Windows.GridEX.GridEX dglistDestination;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox8;
        private Janus.Windows.GridEX.GridEX dgListSource;
        private Janus.Windows.EditControls.UIButton btnBackTo;
        private Janus.Windows.EditControls.UIButton btnTransfer;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuong;
        private System.Windows.Forms.Label label12;
        private Janus.Windows.EditControls.UIRadioButton rdSplit;
        private Janus.Windows.EditControls.UIRadioButton rdMerge;
        private Janus.Windows.EditControls.UIComboBox cbbTableMerge;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.EditControls.UIComboBox cbbTableSplit;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.EditControls.UIButton btnSave;
        private Janus.Windows.EditControls.UIButton btnCancel;
        private System.Windows.Forms.ErrorProvider errorProvider;
    }
}