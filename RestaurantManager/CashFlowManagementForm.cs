﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using System.Reflection;
using System.Globalization;

namespace RestaurantManager
{
    public partial class CashFlowManagementForm : BaseForm
    {
        public string where;
        public CashFlowManagementForm()
        {
            InitializeComponent();
        }

        private void CashFlowManagementForm_Load(object sender, EventArgs e)
        {
            btnSearch_Click(null, null);
        }
        private string GetSearchWhere()
        {
            try
            {
                where = " 1 = 1";
                if (!String.IsNullOrEmpty(txtTenPhongBan.Text))
                {
                    where += " AND PhongBan LIKE N'%" + txtTenPhongBan.Text + "%'";
                }
                where += " AND ThoiGianThanhToan BETWEEN '" + dateTuNgay.Value.ToString("yyyy-MM-dd 00:00:00") + "' AND '" + dateDenNgay.Value.ToString("yyyy-MM-dd 23:59:59") + "'";

                return where;
            }
            catch (Exception ex)
            {
                return null;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private string GetSearchWherePayment()
        {
            try
            {
                where = " 1 = 1";
                where += " AND ThoiGian BETWEEN '" + dateTuNgay.Value.ToString("yyyy-MM-dd 00:00:00") + "' AND '" + dateDenNgay.Value.ToString("yyyy-MM-dd 23:59:59") + "'";

                return where;
            }
            catch (Exception ex)
            {
                return null;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable DataTableHoaDon = HoaDon.SelectDynamic(GetSearchWhere(), null).Tables[0];

                List<HoaDon> HoaDonCollection = new List<HoaDon>();
                HoaDonCollection = ConvertDataTable<HoaDon>(DataTableHoaDon);

                decimal tongThu = 0;
                foreach (var item in HoaDonCollection)
                {
                    tongThu += item.TongTien;
                }

                DataTable DataTablePayment = Payment.SelectDynamic(GetSearchWherePayment(), null).Tables[0];
                List<Payment> PaymentCollection = new List<Payment>();
                PaymentCollection = ConvertDataTable<Payment>(DataTablePayment);

                decimal tongChi = 0;
                foreach (var item in PaymentCollection)
                {
                    tongChi += item.GiaTri;
                }

                CultureInfo cultureInfo = CultureInfo.GetCultureInfo("vi-VN");
                string TongThu = decimal.Parse(tongThu.ToString()).ToString("#,###", cultureInfo.NumberFormat);
                lblTongThu.Text = TongThu;

                string TongChi = decimal.Parse(tongChi.ToString()).ToString("#,###", cultureInfo.NumberFormat);
                lblTongChi.Text = TongChi;

                string TonQuy = decimal.Parse((tongThu - tongChi).ToString()).ToString("#,###", cultureInfo.NumberFormat);
                lblTonQuy.Text = TonQuy;

                dgList.Refetch();
                dgList.DataSource = DataTableHoaDon;
                dgList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private static List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }

        private static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    decimal TongTien = (decimal)e.Row.Cells["TongTien"].Value;
                    e.Row.Cells["TongTien"].Text = TongTien.ToString("#,##0");
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    long id = Convert.ToInt64(e.Row.Cells["Id"].Value);
                    HoaDon HoaDon = HoaDon.Load(id);
                    HoaDon.HangHoaCollection = HoaDon_HangHoa.SelectCollectionBy_HoaDonId(HoaDon.Id);
                    InvoicesForm f = new InvoicesForm();
                    f.HoaDon = HoaDon;
                    f.ShowDialog(this);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgList.CurrentRow.RowType == RowType.Record)
                {
                    int id = System.Convert.ToInt32(dgList.CurrentRow.Cells["Id"].Value.ToString());
                    if (ShowMessage("Bạn có chắc chắn muốn xóa hoá đơn này không?", true, false) == "Yes")
                    {
                        HoaDon HoaDon = HoaDon.Load(id);
                        HoaDon.HangHoaCollection = HoaDon_HangHoa.SelectCollectionBy_HoaDonId(HoaDon.Id);
                        HoaDon.DeleteFull();
                        ShowMessage("Xóa thành công. ", false, false);
                    }
                    else
                        ShowMessage("Xóa không thành công. ", false, false);
                }
                btnSearch_Click(null, null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnSendEmailReport_Click(object sender, EventArgs e)
        {
            try
            {
                if (ShowMessage("Bạn có chắc chắn muốn gửi Email báo cáo này không?", true, false) == "Yes")
                {
                    Helpers help = new Helpers();
                    help.SendEmmailReportTotal(dateTuNgay.Value, dateDenNgay.Value);
                    ShowMessage("Gửi báo cáo thành công. ", false, false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
