﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;

namespace RestaurantManager
{
    public partial class TableAndRoomManagementForm : BaseForm
    {
        public string where;
        public TableAndRoomManagementForm()
        {
            InitializeComponent();
        }

        private void TableAndRoomManagementForm_Load(object sender, EventArgs e)
        {
            LoadKhuVuc();
            btnSearch_Click(null, null);
        }

        private void LoadKhuVuc()
        {
            try
            {
                cbbKhuVuc.DataSource = KhuVuc.SelectAll().Tables[0];
                cbbKhuVuc.DisplayMember = "Ten";
                cbbKhuVuc.ValueMember = "Id";
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private string GetSearchWhere()
        {
            try
            {
                where = " 1 = 1";
                if (!String.IsNullOrEmpty(txtTenPhongBan.Text))
                {
                    where += " AND TenPhongBan LIKE N'%" + txtTenPhongBan.Text + "%'";
                }
                if (cbbKhuVuc.SelectedValue!=null)
                {
                    where += " AND KhuVucId = '" + cbbKhuVuc.SelectedValue + "'";
                }
                return where;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                dgList.Refetch();
                dgList.DataSource = PhongBan.SelectDynamic(GetSearchWhere(), null).Tables[0];
                dgList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgList.CurrentRow.RowType == RowType.Record)
                {
                    int id = System.Convert.ToInt32(dgList.CurrentRow.Cells["Id"].Value.ToString());
                    if (ShowMessage("Bạn có chắc chắn muốn xóa phòng bàn này không?", true, false) == "Yes")
                    {
                        PhongBan PhongBan = PhongBan.Load(id);
                        PhongBan.Delete();
                        ShowMessage("Xóa thành công. ", false, false);
                    }
                    else
                        ShowMessage("Xóa không thành công. ", false, false);
                }
                btnSearch_Click(null, null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    long id = Convert.ToInt64(e.Row.Cells["Id"].Value);
                    PhongBan PhongBan = PhongBan.Load(id);
                    TableAndRoomForm f = new TableAndRoomForm();
                    f.PhongBan = PhongBan;
                    f.ShowDialog(this);
                }
                btnSearch_Click(null, null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    long KhuVucIdId = (long)e.Row.Cells["KhuVucId"].Value;

                    e.Row.Cells["KhuVucId"].Text = KhuVuc.Load(KhuVucIdId).Ten;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
