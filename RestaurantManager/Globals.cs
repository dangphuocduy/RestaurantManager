﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RestaurantManager
{
    public class Globals
    {
        public static string FormatNumber(int number)
        {
            return FormatNumber(number, false);

        }
        public static string FormatNumber(int number, bool batBuocSoThapPhan)
        {
            return "{0:###,###,##0" + Globals.GetDot(number, batBuocSoThapPhan) + "}";
        }
        /// <summary>
        /// Định dạng số thập phân
        /// chuẩn microsoft "###,###,###.########"
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        public static String GetPrecision(int num)
        {
            return GetPrecision(num, false);
        }
        public static String GetPrecision(int num, bool batBuocSoThapPhan)
        {
            string val = "###,###,##0" + GetDot(num, batBuocSoThapPhan);
            return val;
        }
        public static string GetDot(int decimalFormat, bool batBuocSoThapPhan)
        {
            string ret = string.Empty;
            if (decimalFormat < 0) return ret;
            else if (decimalFormat > 0)
                ret += ".";
            for (int i = 0; i < decimalFormat; i++)
            {
                if (!batBuocSoThapPhan)
                    ret += "#";
                else
                    ret += "0";
            }
            return ret;
        }

    }
}
