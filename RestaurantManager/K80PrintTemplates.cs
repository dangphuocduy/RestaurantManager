using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;

namespace RestaurantManager
{
    public partial class K80PrintTemplates : DevExpress.XtraReports.UI.XtraReport
    {
        public PhongBan PhongBan;
        public HoaDon HoaDon;
        public int ThueGTGT;
        public decimal Tax;
        decimal Total;
        decimal TotalTax;
        public K80PrintTemplates()
        {
            InitializeComponent();
        }

        public void BindReport()
        {
            try
            {
                if (PhongBan != null)
                {
                    GlobalSettings.Refreskey();
                    lblTenNhaHang.Text = GlobalSettings.RESTAURANTNAME;
                    lblDiaChi.Text = GlobalSettings.ADDRESS;
                    lblSoDienThoai.Text = GlobalSettings.PHONENUMBER;
                    lblLoaiPhieu.Text = GlobalSettings.LOAIPHIEU;
                    lblNhanVien.Text = GlobalSettings.TENNHANVIEN;

                    lblNote1.Text = GlobalSettings.NOTE1;
                    lblNote2.Text = GlobalSettings.NOTE2;
                    lblNote3.Text = GlobalSettings.NOTE3;
                    lblNote4.Text = GlobalSettings.NOTE4;
                    lblTenPhongBan.Text = PhongBan.TenPhongBan;
                    lblTenKhachHang.Text = PhongBan.KhachHang;
                    lblGioVao.Text = PhongBan.ThoiGianTao.ToString("dd/MM/yyyy HH:mm:ss");
                    lblGioThanhToan.Text = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                    lblTenThue.Text = GlobalSettings.TENTHUE;
                    if (PhongBan.HangHoaCollection.Count > 0)
                    {
                        int k = 1;
                        foreach (var item in PhongBan.HangHoaCollection)
                        {
                            item.STT = k;
                            k++;
                            Total += item.ThanhTien;
                        }
                        if (ThueGTGT != 0)
                        {
                            Tax = (Total * ThueGTGT) / 100;
                            TotalTax = Total + Tax;
                            lblToTal.Text = Total.ToString("N0"); //Total.ToString("0.##");
                            lblTax.Text = Tax.ToString("N0"); //Total.ToString("0.##");
                            lblToTalTax.Text = TotalTax.ToString("N0"); //Total.ToString("0.##");
                            lblNote1.Text = String.Empty;
                        }
                        else
                        {
                            lblToTal.Text = Total.ToString("N0"); //Total.ToString("0.##");
                            lblToTalTax.Text = Total.ToString("N0"); //Total.ToString("0.##");
                        }
                        this.DetailReport.DataSource = PhongBan.HangHoaCollection;
                        lblSTT.DataBindings.Add("Text", this.DetailReport.DataSource, "STT");
                        lblTenHangHoa.DataBindings.Add("Text", this.DetailReport.DataSource, "TenHangHoa");
                        //lblDVT.DataBindings.Add("Text", this.DetailReport.DataSource, "DonViTinh");
                        lblSoLuong.DataBindings.Add("Text", this.DetailReport.DataSource, "SoLuong", "{0:0.##}");
                        lblDonGia.DataBindings.Add("Text", this.DetailReport.DataSource, "DonGia", "{0:#,0}");
                        lblThanhTien.DataBindings.Add("Text", this.DetailReport.DataSource, "ThanhTien", "{0:#,0}");
                    }
                }
                else if (HoaDon != null)
                {
                    GlobalSettings.Refreskey();
                    lblTenNhaHang.Text = GlobalSettings.RESTAURANTNAME;
                    lblDiaChi.Text = GlobalSettings.ADDRESS;
                    lblSoDienThoai.Text = GlobalSettings.PHONENUMBER;
                    lblLoaiPhieu.Text = GlobalSettings.HOADON;
                    lblNhanVien.Text = GlobalSettings.TENNHANVIEN;

                    lblNote1.Text = GlobalSettings.NOTE1;
                    lblNote2.Text = GlobalSettings.NOTE2;
                    lblNote3.Text = GlobalSettings.NOTE3;
                    lblNote4.Text = GlobalSettings.NOTE4;
                    lblTenPhongBan.Text = HoaDon.PhongBan;
                    lblGioVao.Text = HoaDon.ThoiGianTao.ToString("dd/MM/yyyy HH:mm:ss");
                    lblGioThanhToan.Text = HoaDon.ThoiGianThanhToan.ToString("dd/MM/yyyy HH:mm:ss");
                    lblTenThue.Text = GlobalSettings.TENTHUE;
                    if (HoaDon.HangHoaCollection.Count > 0)
                    {
                        int k = 1;
                        foreach (var item in HoaDon.HangHoaCollection)
                        {
                            item.STT = k;
                            Total += item.ThanhTien;
                            k++;
                        }
                        if (HoaDon.ThueSuat != null)
                        {
                            if (HoaDon.ThueSuat != 0)
                            {

                                lblToTal.Text = Total.ToString("N0"); //Total.ToString("0.##");
                                lblTax.Text = (Total * HoaDon.ThueSuat / 100).ToString("N0"); //Total.ToString("0.##");
                                lblToTalTax.Text = HoaDon.TongTien.ToString("N0"); //Total.ToString("0.##");
                                lblNote1.Text = String.Empty;
                            }
                            else
                            {
                                lblToTal.Text = HoaDon.TongTien.ToString("N0"); //Total.ToString("0.##");
                                lblToTalTax.Text = HoaDon.TongTien.ToString("N0"); //Total.ToString("0.##");
                            }
                        }
                        else
                        {
                            lblToTal.Text = HoaDon.TongTien.ToString("N0"); //Total.ToString("0.##");
                            lblToTalTax.Text = HoaDon.TongTien.ToString("N0"); //Total.ToString("0.##");
                        }

                        this.DetailReport.DataSource = HoaDon.HangHoaCollection;
                        lblSTT.DataBindings.Add("Text", this.DetailReport.DataSource, "STT");
                        lblTenHangHoa.DataBindings.Add("Text", this.DetailReport.DataSource, "TenHangHoa");
                        //lblDVT.DataBindings.Add("Text", this.DetailReport.DataSource, "DonViTinh");
                        lblSoLuong.DataBindings.Add("Text", this.DetailReport.DataSource, "SoLuong", "{0:0.##}");
                        lblDonGia.DataBindings.Add("Text", this.DetailReport.DataSource, "DonGia", "{0:#,0}");
                        lblThanhTien.DataBindings.Add("Text", this.DetailReport.DataSource, "ThanhTien", "{0:#,0}");
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
