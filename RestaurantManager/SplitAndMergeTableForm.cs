﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using System.Globalization;

namespace RestaurantManager
{
    public partial class SplitAndMergeTableForm : BaseForm
    {
        public PhongBan PhongBanSource;
        public PhongBan PhongBanDestination;
        public PhongBan_HangHoa HangHoaSource;
        public PhongBan_HangHoa HangHoaDestination;
        public long phongBanSourceId;
        public long phongBanDestinationId;
        public SplitAndMergeTableForm()
        {
            InitializeComponent();
        }
        private void LoadTable()
        {
            try
            {
                cbbTableSplit.DataSource = PhongBan.SelectAll().Tables[0];
                cbbTableSplit.DisplayMember = "TenPhongBan";
                cbbTableSplit.ValueMember = "Id";

                cbbTableMerge.DataSource = PhongBan.SelectAll().Tables[0];
                cbbTableMerge.DisplayMember = "TenPhongBan";
                cbbTableMerge.ValueMember = "Id";
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void rdMerge_CheckedChanged(object sender, EventArgs e)
        {
            if (rdMerge.Checked)
            {
                tabSplit.Hide();
                tabSplit.Enabled = false;
                tabMerge.Show();
                tabMerge.Enabled = true;
            }
            else
            {
                tabSplit.Show();
                tabSplit.Enabled = true;
                tabMerge.Hide();
                tabMerge.Enabled = false;
            }
        }

        private void rdSplit_CheckedChanged(object sender, EventArgs e)
        {
            if (rdSplit.Checked)
            {
                tabSplit.Show();
                tabSplit.Enabled = true;
                tabMerge.Hide();
                tabMerge.Enabled = false;
            }
            else
            {
                tabSplit.Hide();
                tabSplit.Enabled = false;
                tabMerge.Show();
                tabMerge.Enabled = true;
            }
        }

        private void SplitAndMergeTableForm_Load(object sender, EventArgs e)
        {
            rdMerge.Checked = true;
            LoadTable();
            if (PhongBanSource != null)
            {
                phongBanSourceId = PhongBanSource.Id;
                LoadHangHoaSource();
                LoadHangHoaMerge();
                this.Text = "TÁCH GHÉP BÀN : " + PhongBanSource.TenPhongBan.ToUpper();
            }
        }
        private void LoadHangHoaSource()
        {
            try
            {
                dgListSource.Refetch();
                dgListSource.DataSource = PhongBanSource.HangHoaCollection;
                dgListSource.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void LoadHangHoaMerge()
        {
            try
            {
                dgListMerge.Refetch();
                dgListMerge.DataSource = PhongBanSource.HangHoaCollection;
                dgListMerge.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void LoadHangHoaDestination()
        {
            try
            {
                dglistDestination.Refetch();
                dglistDestination.DataSource = PhongBanDestination.HangHoaCollection;
                dglistDestination.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgListMerge_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    decimal SoLuong = (decimal)e.Row.Cells["SoLuong"].Value;
                    decimal DonGia = (decimal)e.Row.Cells["DonGia"].Value;
                    decimal ThanhTien = (decimal)e.Row.Cells["ThanhTien"].Value;
                    e.Row.Cells["SoLuong"].Text = ToTrimmedString(SoLuong);
                    e.Row.Cells["DonGia"].Text = DonGia.ToString("#,##0");
                    e.Row.Cells["ThanhTien"].Text = ThanhTien.ToString("#,##0");
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public string ToTrimmedString(decimal num)
        {
            try
            {
                string str = num.ToString();
                string decimalSeparator = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
                if (str.Contains(decimalSeparator))
                {
                    str = str.TrimEnd('0');
                    if (str.EndsWith(decimalSeparator))
                    {
                        str = str.Remove(str.Length - 1, 1);
                    }
                }
                return str;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return null;
        }

        public string RemoveFromEnd(string str, int characterCount)
        {
            try
            {
                return str.Remove(str.Length - characterCount, characterCount);

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        private void dgListSource_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    decimal SoLuong = (decimal)e.Row.Cells["SoLuong"].Value;
                    e.Row.Cells["SoLuong"].Text = ToTrimmedString(SoLuong);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgListSource_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = dgListSource.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HangHoaSource = new PhongBan_HangHoa();
                        HangHoaSource = (PhongBan_HangHoa)i.GetRow().DataRow;
                        txtSoLuong.Text = HangHoaSource.SoLuong.ToString();
                    }

                }
                //if (dgListSource.CurrentRow.RowType == RowType.Record)
                //{
                //    int id = System.Convert.ToInt32(dgListSource.CurrentRow.Cells["Id"].Value.ToString());
                //    HangHoaSource = PhongBan_HangHoa.Load(id);
                //    txtSoLuong.Text = HangHoaSource.SoLuong.ToString();
                //}
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dglistDestination_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = dglistDestination.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HangHoaDestination = new PhongBan_HangHoa();
                        HangHoaDestination = (PhongBan_HangHoa)i.GetRow().DataRow;
                        txtSoLuong.Text = HangHoaDestination.SoLuong.ToString();
                    }

                }

                //if (dglistDestination.CurrentRow.RowType == RowType.Record)
                //{
                //    int id = System.Convert.ToInt32(dglistDestination.CurrentRow.Cells["Id"].Value.ToString());
                //    HangHoaDestination = PhongBan_HangHoa.Load(id);
                //    txtSoLuong.Text = HangHoaDestination.SoLuong.ToString();
                //}
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dglistDestination_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    decimal SoLuong = (decimal)e.Row.Cells["SoLuong"].Value;
                    e.Row.Cells["SoLuong"].Text = ToTrimmedString(SoLuong);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private bool ValidateFormMerge(bool isOnlyWarning)
        {
            bool isValid = true;
            isValid &= ValidateControl.ValidateChoose(cbbTableMerge, errorProvider, "Phòng bàn", isOnlyWarning);
            return isValid;
        }

        private bool ValidateFormSplit(bool isOnlyWarning)
        {
            bool isValid = true;
            isValid &= ValidateControl.ValidateChoose(cbbTableSplit, errorProvider, "Phòng bàn", isOnlyWarning);
            return isValid;
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (rdMerge.Checked)
                {
                    if (!ValidateFormMerge(false))
                        return;
                    var phongBanId = Convert.ToInt64(cbbTableMerge.SelectedValue);
                    if (phongBanSourceId == phongBanId)
                    {
                        ShowMessage("Phòng bàn chọn đang trùng với phòng bàn hiện tại cần ghép !", false);
                        return;
                    }
                    PhongBan PhongBanMerged = PhongBan.Load(phongBanId);
                    PhongBanMerged.HangHoaCollection = PhongBan_HangHoa.SelectCollectionBy_PhongBanId(PhongBanMerged.Id);
                    bool isAdd = true;
                    List<PhongBan_HangHoa> HangHoaAdd = new List<PhongBan_HangHoa>();
                    if (PhongBanMerged.HangHoaCollection.Count > 0)
                    {
                        foreach (var item in PhongBanSource.HangHoaCollection)
                        {
                            isAdd = true;
                            foreach (var ite in PhongBanMerged.HangHoaCollection)
                            {
                                if (item.MaHangHoa == ite.MaHangHoa)
                                {
                                    ite.SoLuong += ite.SoLuong;
                                    ite.ThanhTien = item.SoLuong * item.DonGia;
                                    isAdd = false;
                                    break;
                                }
                            }
                            if (isAdd)
                            {
                                HangHoaAdd.Add(item);
                            }
                        }

                        foreach (var item in PhongBanSource.HangHoaCollection)
                        {
                            item.Delete();
                        }

                        if (HangHoaAdd.Count > 0)
                        {
                            foreach (var item in HangHoaAdd)
                            {
                                item.Id = 0;
                                PhongBanMerged.HangHoaCollection.Add(item);
                            }
                        }
                    }
                    else
                    {
                        foreach (var item in PhongBanSource.HangHoaCollection)
                        {
                            item.Delete();
                        }
                        foreach (var item in PhongBanSource.HangHoaCollection)
                        {
                            item.Id = 0;
                            PhongBanMerged.HangHoaCollection.Add(item);
                        }
                    }
                    PhongBanMerged.InsertUpdateFull();
                    ShowMessage("Ghép thành công", false, false);
                    this.Close();
                }
                else
                {
                    if (!ValidateFormSplit(false))
                        return;

                    if (PhongBanDestination.HangHoaCollection.Count == 0)
                    {
                        ShowMessage("Không có danh sách hàng hoá cần tách đơn !", false);
                        return;
                    }
                    if (phongBanSourceId == phongBanDestinationId)
                    {
                        ShowMessage("Phòng bàn chọn đang trùng với phòng bàn hiện tại cần tách !", false);
                        return;
                    }
                    List<PhongBan_HangHoa> HangHoaSoureCollection = new List<PhongBan_HangHoa>();
                    HangHoaSoureCollection = PhongBanSource.HangHoaCollection;

                    foreach (var item in HangHoaSoureCollection)
                    {
                        item.Id = 0;
                    }

                    PhongBanSource.HangHoaCollection = PhongBan_HangHoa.SelectCollectionBy_PhongBanId(phongBanSourceId);

                    foreach (var item in PhongBanSource.HangHoaCollection)
                    {
                        item.Delete();
                    }
                    PhongBanSource.HangHoaCollection = HangHoaSoureCollection;
                    PhongBanSource.InsertUpdateFull();

                    List<PhongBan_HangHoa> HangHoaDestinationCollection = new List<PhongBan_HangHoa>();
                    HangHoaDestinationCollection = PhongBanDestination.HangHoaCollection;

                    foreach (var item in HangHoaDestinationCollection)
                    {
                        item.Id = 0;
                    }

                    PhongBanDestination.HangHoaCollection = PhongBan_HangHoa.SelectCollectionBy_PhongBanId(phongBanDestinationId);

                    foreach (var item in PhongBanDestination.HangHoaCollection)
                    {
                        item.Delete();
                    }

                    PhongBanDestination.HangHoaCollection = HangHoaDestinationCollection;
                    PhongBanDestination.InsertUpdateFull();

                    ShowMessage("Tách thành công", false, false);
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            isValid &= ValidateControl.ValidateNull(txtSoLuong, errorProvider, "Số lượng", isOnlyWarning);
            isValid &= ValidateControl.ValidateZero(txtSoLuong, errorProvider, "Số lượng");
            isValid &= ValidateControl.ValidateChoose(cbbTableSplit, errorProvider, "Phòng bàn");
            return isValid;
        }

        private void btnTransfer_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                if (HangHoaSource != null)
                {
                    bool isAdd = true;
                    decimal SoLuong = Convert.ToDecimal(txtSoLuong.Text);
                    var maHangHoa = HangHoaSource.MaHangHoa;
                    if (SoLuong > HangHoaSource.SoLuong)
                    {
                        errorProvider.SetError(txtSoLuong, "Số lượng chuyển phải nhỏ hơn hoặc bằng số lượng hiện tại!");
                        return;
                    }
                    if (PhongBanDestination.HangHoaCollection.Count > 0)
                    {
                        foreach (var item in PhongBanDestination.HangHoaCollection)
                        {
                            if (item.MaHangHoa == maHangHoa)
                            {
                                item.SoLuong += SoLuong;
                                item.ThanhTien += item.SoLuong * item.DonGia;
                                isAdd = false;
                                break;
                            }
                        }
                        foreach (var item in PhongBanSource.HangHoaCollection)
                        {
                            if (item.MaHangHoa == maHangHoa)
                            {
                                item.SoLuong -= SoLuong;
                                break;
                            }
                        }
                        if (isAdd)
                        {
                            PhongBan_HangHoa hanghoa = new PhongBan_HangHoa();
                            hanghoa.PhongBanId = PhongBanDestination.Id;
                            hanghoa.MaHangHoa = HangHoaSource.MaHangHoa;
                            hanghoa.TenHangHoa = HangHoaSource.TenHangHoa;
                            hanghoa.NhomHangHoaId = HangHoaSource.NhomHangHoaId;
                            hanghoa.LoaiThucDonId = HangHoaSource.LoaiThucDonId;
                            hanghoa.SoLuong = SoLuong;
                            hanghoa.DonGia = HangHoaSource.DonGia;
                            hanghoa.ThanhTien = HangHoaSource.ThanhTien;
                            hanghoa.DonViTinh = HangHoaSource.DonViTinh;

                            PhongBanDestination.HangHoaCollection.Add(hanghoa);
                        }
                        var hangHoaRemove = PhongBanSource.HangHoaCollection.SingleOrDefault(x => x.SoLuong == 0);
                        if (hangHoaRemove != null)
                            PhongBanSource.HangHoaCollection.Remove(hangHoaRemove);
                    }
                    else
                    {
                        PhongBan_HangHoa hanghoa = new PhongBan_HangHoa();
                        hanghoa.PhongBanId = PhongBanDestination.Id;
                        hanghoa.MaHangHoa = HangHoaSource.MaHangHoa;
                        hanghoa.TenHangHoa = HangHoaSource.TenHangHoa;
                        hanghoa.NhomHangHoaId = HangHoaSource.NhomHangHoaId;
                        hanghoa.LoaiThucDonId = HangHoaSource.LoaiThucDonId;
                        hanghoa.SoLuong = SoLuong;
                        hanghoa.DonGia = HangHoaSource.DonGia;
                        hanghoa.ThanhTien = HangHoaSource.ThanhTien;
                        hanghoa.DonViTinh = HangHoaSource.DonViTinh;

                        PhongBanDestination.HangHoaCollection.Add(hanghoa);

                        foreach (var item in PhongBanSource.HangHoaCollection)
                        {
                            if (maHangHoa == item.MaHangHoa)
                            {
                                item.SoLuong -= SoLuong;
                                break;
                            }
                        }

                        var hangHoaRemove = PhongBanSource.HangHoaCollection.SingleOrDefault(x => x.SoLuong == 0);
                        if (hangHoaRemove != null)
                            PhongBanSource.HangHoaCollection.Remove(hangHoaRemove);
                    }

                    LoadHangHoaSource();
                    LoadHangHoaDestination();
                }

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnBackTo_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                if (HangHoaDestination != null)
                {
                    bool isAdd = true;
                    decimal SoLuong = Convert.ToDecimal(txtSoLuong.Text);
                    var maHangHoa = HangHoaDestination.MaHangHoa;
                    if (SoLuong > HangHoaDestination.SoLuong)
                    {
                        errorProvider.SetError(txtSoLuong, "Số lượng chuyển phải nhỏ hơn hoặc bằng số lượng hiện tại!");
                        return;
                    }
                    if (PhongBanSource.HangHoaCollection.Count > 0)
                    {
                        foreach (var item in PhongBanSource.HangHoaCollection)
                        {
                            if (item.MaHangHoa == maHangHoa)
                            {
                                item.SoLuong += SoLuong;
                                item.ThanhTien += item.SoLuong * item.DonGia;
                                isAdd = false;
                                break;
                            }
                        }
                        foreach (var item in PhongBanDestination.HangHoaCollection)
                        {
                            if (item.MaHangHoa == maHangHoa)
                            {
                                item.SoLuong -= SoLuong;
                                break;
                            }
                        }
                        if (isAdd)
                        {
                            PhongBan_HangHoa hanghoa = new PhongBan_HangHoa();
                            hanghoa.PhongBanId = PhongBanSource.Id;
                            hanghoa.MaHangHoa = HangHoaDestination.MaHangHoa;
                            hanghoa.TenHangHoa = HangHoaDestination.TenHangHoa;
                            hanghoa.NhomHangHoaId = HangHoaDestination.NhomHangHoaId;
                            hanghoa.LoaiThucDonId = HangHoaDestination.LoaiThucDonId;
                            hanghoa.SoLuong = SoLuong;
                            hanghoa.DonGia = HangHoaDestination.DonGia;
                            hanghoa.ThanhTien = HangHoaDestination.ThanhTien;
                            hanghoa.DonViTinh = HangHoaDestination.DonViTinh;

                            PhongBanSource.HangHoaCollection.Add(hanghoa);
                        }
                        var hangHoaRemove = PhongBanDestination.HangHoaCollection.SingleOrDefault(x => x.SoLuong == 0);
                        if (hangHoaRemove != null)
                            PhongBanDestination.HangHoaCollection.Remove(hangHoaRemove);
                    }
                    else
                    {
                        PhongBan_HangHoa hanghoa = new PhongBan_HangHoa();
                        hanghoa.PhongBanId = PhongBanSource.Id;
                        hanghoa.MaHangHoa = HangHoaDestination.MaHangHoa;
                        hanghoa.TenHangHoa = HangHoaDestination.TenHangHoa;
                        hanghoa.NhomHangHoaId = HangHoaDestination.NhomHangHoaId;
                        hanghoa.LoaiThucDonId = HangHoaDestination.LoaiThucDonId;
                        hanghoa.SoLuong = SoLuong;
                        hanghoa.DonGia = HangHoaDestination.DonGia;
                        hanghoa.ThanhTien = HangHoaDestination.ThanhTien;
                        hanghoa.DonViTinh = HangHoaDestination.DonViTinh;

                        PhongBanSource.HangHoaCollection.Add(hanghoa);

                        foreach (var item in PhongBanDestination.HangHoaCollection)
                        {
                            if (maHangHoa == item.MaHangHoa)
                            {
                                item.SoLuong -= SoLuong;
                                break;
                            }
                        }

                        var hangHoaRemove = PhongBanDestination.HangHoaCollection.SingleOrDefault(x => x.SoLuong == 0);
                        if (hangHoaRemove != null)
                            PhongBanDestination.HangHoaCollection.Remove(hangHoaRemove);
                    }

                    LoadHangHoaSource();
                    LoadHangHoaDestination();
                }

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void cbbTableSplit_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                var phongBanId = Convert.ToInt64(cbbTableSplit.SelectedValue);
                phongBanDestinationId = phongBanId;
                PhongBanDestination = PhongBan.Load(phongBanId);
                PhongBanDestination.HangHoaCollection = PhongBan_HangHoa.SelectCollectionBy_PhongBanId(PhongBanDestination.Id);
                LoadHangHoaDestination();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtSoLuong_TextChanged(object sender, EventArgs e)
        {
            //try
            //{
            //    var soluong = Convert.ToDecimal(txtSoLuong.Text);
            //    if (HangHoaSource != null)
            //    {
            //        if (soluong > HangHoaSource.SoLuong)
            //        {
            //            errorProvider.SetError(txtSoLuong, "Số lượng không được lớn hơn số lượng hiện tại!");
            //        }
            //    }
            //    if (HangHoaDestination != null)
            //    {
            //        if (soluong > HangHoaDestination.SoLuong)
            //        {
            //            errorProvider.SetError(txtSoLuong, "Số lượng không được lớn hơn số lượng hiện tại!");
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Logger.LocalLogger.Instance().WriteMessage(ex);
            //}
        }
    }
}
