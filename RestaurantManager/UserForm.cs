﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Security.Cryptography;
using RestaurantManager;

namespace ManagementPrintFromExcel
{
    public partial class UserForm : BaseForm
    {
       public User user;
       public bool isAdd;
       public bool isChange;
       public bool isView;
        public UserForm()
        {
            InitializeComponent();
        }

        private void UserForm_Load(object sender, EventArgs e)
        {
            if (user!=null)
            {
                if (isView)
                {
                    txtUserName.Enabled = false;
                    txtFullName.Enabled = false;
                    txtDiaChi.Enabled = false;
                    txtSoDienThoai.Enabled = false;
                    txtEmail.Enabled = false;
                    chkIsAdmin.Enabled = false;
                    btnUpdate.Enabled = false;
                }
                txtUserName.Text = user.UserName;
                txtFullName.Text = user.FullName;
                txtDiaChi.Text = user.DiaChi;
                txtSoDienThoai.Text = user.SoDienThoai;
                txtEmail.Text = user.Email;
                if (Convert.ToBoolean(user.isAdmin))
                {
                    chkIsAdmin.CheckState = CheckState.Checked;
                }
                grbAdd.Enabled = false;
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (user==null)
                {
                    user = new User();
                }
                if (!ValidateForm(false))
                    return;
                user.UserName = txtUserName.Text.Trim();
                user.FullName = txtFullName.Text.Trim();
                user.isAdmin = Convert.ToBoolean(chkIsAdmin.Checked);
                user.DiaChi = txtDiaChi.Text;
                user.SoDienThoai = txtSoDienThoai.Text;
                user.Email = txtEmail.Text;
                if (user.ID == 0)
                {
                    //if (txtUserName.Text.Length == 0)
                    //{
                    //    ShowMessage("Nhập tên đăng nhập", false, false);
                    //    //MessageBox.Show("Nhập tên đăng nhập ", "Thông báo hệ thống", MessageBoxButtons.OK);
                    //    return;
                    //}
                    //if (txtPasswordNew.Text.Length == 0)
                    //{
                    //    ShowMessage("Nhập mật khẩu mới ", false, false);
                    //    //MessageBox.Show("Nhập mật khẩu mới ", "Thông báo hệ thống", MessageBoxButtons.OK);
                    //    return;
                    //}
                    if (user.CheckUserName(txtUserName.Text.Trim()))
                    {
                        ShowMessage("Tên người dùng đã có .Bạn hãy nhập tên khác ", false, false);
                        //MessageBox.Show("Tên người dùng đã có .Bạn hãy nhập tên khác ", "Thông báo hệ thống", MessageBoxButtons.OK);
                        return;
                    }
                    if (txtPasswordNew.Text.Trim().Length > 0)
                    {
                        if (txtPasswordNew.Text.Trim().ToUpper() != txtRePassword.Text.Trim().ToUpper())
                        {
                            ShowMessage("Nhập mật khẩu không giống nhau", false, false);
                            //MessageBox.Show("Nhập mật khẩu không giống nhau ", "Thông báo hệ thống", MessageBoxButtons.OK);
                            return;
                        }
                    }
                    user.Password = EncryptPassword(txtPasswordNew.Text.Trim());
                    user.Insert();
                    ShowMessage("Tạo mới người dùng thành công", false, false);
                    //MessageBox.Show("Tạo mới người dùng thành công ", "Thông báo hệ thống", MessageBoxButtons.OK);
                    this.Close();
                }
                else
                {
                    user.Update();
                    ShowMessage("Cập nhật thông tin thành công", false, false);
                    //MessageBox.Show("Cập nhật thông tin thành công ", "Thông báo hệ thống", MessageBoxButtons.OK);
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Lỗi\n " + ex.Message, "Thông báo hệ thống", MessageBoxButtons.OK);
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //ShowMessage("Có lỗi trong quá trình xử lý thông tin \r\n" + ex.Message, false, true);
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            isValid &= ValidateControl.ValidateNull(txtFullName, errorProvider, "Tên người dùng ", isOnlyWarning);
            isValid &= ValidateControl.ValidateNull(txtDiaChi, errorProvider, "Địa chỉ ", isOnlyWarning);
            isValid &= ValidateControl.ValidateNull(txtSoDienThoai, errorProvider, "Số điện thoại ", isOnlyWarning);
            isValid &= ValidateControl.ValidateNull(txtUserName, errorProvider, "Tên đăng nhập ", isOnlyWarning);
            isValid &= ValidateControl.ValidateNull(txtEmail, errorProvider, "Email ", isOnlyWarning);
            if (grbAdd.Enabled)
            {
                isValid &= ValidateControl.ValidateNull(txtPasswordNew, errorProvider, "Mật khẩu ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtRePassword, errorProvider, "Mật khẩu nhập lại ", isOnlyWarning);
            }
            return isValid;
        }
        private void SaveNodeXmlAppSettings()
        {
            ConfigConecionForm.SaveNodeXmlAppSettings("NGAYSAOLUU", DateTime.Today.ToString("dd/MM/yyyy"));
        }
        public string EncryptPassword(string password)
        {
            UnicodeEncoding encoding = new UnicodeEncoding();
            byte[] hashBytes = encoding.GetBytes(password);

            // compute SHA-1 hash.
            SHA1 sha1 = new SHA1CryptoServiceProvider();
            byte[] bytePassword = sha1.ComputeHash(hashBytes);
            string cryptPassword = Convert.ToBase64String(bytePassword);
            return cryptPassword;
        }
    }
}
