﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ManagementPrintFromExcel;

namespace RestaurantManager
{
    public partial class MainForm : BaseForm
    {
        public static bool isLoginSuccess = false;
        public static Principal QuanTri;
        public static User user;

        public MainForm()
        {
            InitializeComponent();
            GlobalSettings.RefreshKey();
        }

        private void cmdMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdBackupDatabase":
                    this.BackupDatabase();
                    break;
                case "cmdConfigConnection":
                    this.ConfigConnection();
                    break;
                case "cmdUser":
                    this.ManagementUser();
                    break;
                case "cmdChangePassword":
                    this.ChangePassword();
                    break;
                case "cmdLoginUser":
                    this.Logout();
                    break;
                case "cmdExit":
                    if (ShowMessage("Bạn có muốn thoát phần mềm không ?", true, false) == "Yes")
                    {
                        Application.Exit();
                    }
                    break;
                case "cmdConfigSystem":
                    this.ConfigSystem();
                    break;
                case "cmdLog":
                    System.Diagnostics.Process.Start(Application.StartupPath + "\\Error.log");
                    break;
                case "cmdSQLQuery":
                    this.SQLQuery();
                    break;
            }
        }

        private void ManagementUser()
        {
            LoginRoleForm login = new LoginRoleForm();
            login.ShowDialog(this);
            if (LoginRoleForm.IsSuccess == true)
            {
                Form[] forms = this.MdiChildren;
                for (int i = 0; i < forms.Length; i++)
                {
                    if (forms[i].Name.ToString().Equals("ManagementUserForm"))
                    {
                        forms[i].Activate();
                        return;
                    }
                }
                ManagementUserForm f = new ManagementUserForm();
                f.MdiParent = this;
                f.Show();
            }
        }

        private void ChangePassword()
        {
            //Form[] forms = this.MdiChildren;
            //for (int i = 0; i < forms.Length; i++)
            //{
            //    if (forms[i].Name.ToString().Equals("ChangePassword"))
            //    {
            //        forms[i].Activate();
            //        return;
            //    }
            //}
            ChangePassword f = new ChangePassword();
            f.user = user;
            //f.MdiParent = this;
            f.ShowDialog(this);
        }

        private void Logout()
        {
            this.Hide();
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                forms[i].Close();
            }
            MainForm.QuanTri = null;
            isLoginSuccess = false;
            Login login = new Login();
            login.ShowDialog(this);
            if (isLoginSuccess)
            {
                user = User.Load(((SiteIdentity)MainForm.QuanTri.Identity).user.ID);
                this.Show();
            }
            else
            {
                Application.Exit();
            }
        }

        private void SQLQuery()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("SQLQueryForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }

            SQLQueryForm f = new SQLQueryForm();
            f.MdiParent = this;
            f.Show();
        }

        private void ConfigSystem()
        {
            ConfigSystemForm f = new ConfigSystemForm();
            f.ShowDialog(this);
        }

        private void ConfigConnection()
        {
            ConfigConecionForm f = new ConfigConecionForm();
            f.ShowDialog(this);
        }

        private void BackupDatabase()
        {
            BackupAndRestoreForm f = new BackupAndRestoreForm();
            f.ShowDialog(this);
        }

        private void explorerBarTableAndRoom_ItemClick(object sender, Janus.Windows.ExplorerBar.ItemEventArgs e)
        {
            switch (e.Item.Key)
            {
                case "cmdAdd":
                    this.ShowTableAndRoom();
                    break;
                case "cmdManagement":
                    this.ShowTableAndRoomManagement();
                    break;
                case "cmdArea":
                    this.ShowArea();
                    break;
                case "cmdAreaManagement":
                    this.ShowAreaManagement();
                    break;
                default:
                    break;
            }
        }

        private void ShowAreaManagement()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("AreaManagementForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            AreaManagementForm f = new AreaManagementForm();
            f.MdiParent = this;
            f.Show();
        }

        private void ShowArea()
        {
            AreaForm f = new AreaForm();
            //f.MdiParent = this;
            f.ShowDialog(this);
        }

        private void ShowTableAndRoomManagement()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("TableAndRoomManagementForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            TableAndRoomManagementForm f = new TableAndRoomManagementForm();
            f.MdiParent = this;
            f.Show();
        }

        private void ShowTableAndRoom()
        {
            //Form[] forms = this.MdiChildren;
            //for (int i = 0; i < forms.Length; i++)
            //{
            //    if (forms[i].Name.ToString().Equals("TableAndRoomForm"))
            //    {
            //        forms[i].Activate();
            //        return;
            //    }
            //}
            TableAndRoomForm f = new TableAndRoomForm();
            //f.MdiParent = this;
            f.ShowDialog(this);
        }

        private void explorerBarProducts_ItemClick(object sender, Janus.Windows.ExplorerBar.ItemEventArgs e)
        {
            switch (e.Item.Key)
            {
                case "cmdAddCategory":
                    this.ShowCategoryForm();
                    break;
                case "cmdCategoryManagement":
                    this.ShowCategoryManagement();
                    break;
                case "cmdAddProduct":
                    this.ShowProduct();
                    break;
                case "cmdProductManagement":
                    this.ShowProductManagement();
                    break;
                default:
                    break;
            }
        }

        private void ShowProductManagement()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("ProductManagementForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            ProductManagementForm f = new ProductManagementForm();
            f.MdiParent = this;
            f.Show();
        }

        private void ShowProduct()
        {
            //Form[] forms = this.MdiChildren;
            //for (int i = 0; i < forms.Length; i++)
            //{
            //    if (forms[i].Name.ToString().Equals("ProductForm"))
            //    {
            //        forms[i].Activate();
            //        return;
            //    }
            //}
            ProductForm f = new ProductForm();
            //f.MdiParent = this;
            f.ShowDialog(this);
        }
        private void ShowCategoryForm()
        {
            //Form[] forms = this.MdiChildren;
            //for (int i = 0; i < forms.Length; i++)
            //{
            //    if (forms[i].Name.ToString().Equals("CategoryForm"))
            //    {
            //        forms[i].Activate();
            //        return;
            //    }
            //}
            CategoryForm f = new CategoryForm();
            //f.MdiParent = this;
            f.ShowDialog(this);
        }

        private void ShowCategoryManagement()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("CategoryManagementForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            CategoryManagementForm f = new CategoryManagementForm();
            f.MdiParent = this;
            f.Show();
        }

        private void explorerBarCashFlow_ItemClick(object sender, Janus.Windows.ExplorerBar.ItemEventArgs e)
        {
            switch (e.Item.Key)
            {
                case "cmdCashFlowManagement":
                    this.CashFlowManagement();
                    break;
                case "cmdPayment":
                    this.Payment();
                    break;
                case "cmdPaymentType":
                    this.PaymentType();
                    break;
                case "cmdPaymentManagement":
                    this.PaymentManagement();
                    break;
                default:
                    break;
            }
        }

        private void PaymentManagement()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("PaymentManagementForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            PaymentManagementForm f = new PaymentManagementForm();
            f.MdiParent = this;
            f.Show();
        }

        private void PaymentType()
        {
            PaymentTypeForm f = new PaymentTypeForm();
            f.ShowDialog(this);
        }

        private void Payment()
        {
            PaymentForm f = new PaymentForm();
            f.ShowDialog(this);
        }

        private void CashFlowManagement()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("CashFlowManagementForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            CashFlowManagementForm f = new CashFlowManagementForm();
            f.MdiParent = this;
            f.Show();
        }

        private void explorerBarCashier_ItemClick(object sender, Janus.Windows.ExplorerBar.ItemEventArgs e)
        {
            switch (e.Item.Key)
            {
                case "cmdCashier":
                    this.ShowCashier();
                    break;
                default:
                    break;
            }
        }

        private void ShowCashier()
        {
            Form[] forms = this.MdiChildren;
            for (int i = 0; i < forms.Length; i++)
            {
                if (forms[i].Name.ToString().Equals("CashierForm"))
                {
                    forms[i].Activate();
                    return;
                }
            }
            CashierForm f = new CashierForm();
            f.MdiParent = this;
            f.Show();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            Login login = new Login();
            login.ShowDialog(this);
            //string strVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            //this.Text += " - Build " + strVersion;
            if (isLoginSuccess)
            {
                user = User.Load(((SiteIdentity)MainForm.QuanTri.Identity).user.ID);
                this.Show();
                GlobalSettings.Refreskey();

                if (GlobalSettings.NGAYSAOLUU != null && GlobalSettings.NGAYSAOLUU != "")
                {
                    string st = GlobalSettings.NGAYSAOLUU;
                    DateTime time = Convert.ToDateTime(GlobalSettings.NGAYSAOLUU);
                    int NHAC_NHO_SAO_LUU = Convert.ToInt32(GlobalSettings.NHAC_NHO_SAO_LUU);
                    TimeSpan time1 = DateTime.Today.Subtract(time);
                    int ngay = NHAC_NHO_SAO_LUU - time1.Days;
                    if (ngay <= 0)
                        BackupDatabase();
                }
                else
                    BackupDatabase();

                if (!(GlobalSettings.LAST_BACKUP.Equals("")) && !(GlobalSettings.NHAC_NHO_SAO_LUU.Equals("")))
                    if (Convert.ToDateTime(GlobalSettings.LAST_BACKUP).Add(TimeSpan.Parse(GlobalSettings.NHAC_NHO_SAO_LUU)) == DateTime.Today)
                    {
                        BackupDatabase();
                    }
            }
            else
            {
                MainForm.isLoginSuccess = false;
            }
        }
    }
}
