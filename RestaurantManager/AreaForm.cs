﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

namespace RestaurantManager
{
    public partial class AreaForm : BaseForm
    {
        public KhuVuc KhuVuc;
        public AreaForm()
        {
            InitializeComponent();
        }

        private void AreaForm_Load(object sender, EventArgs e)
        {
            try
            {
                if (KhuVuc != null)
                {
                    txtArea.Text = KhuVuc.Ten;
                }
                else
                {
                    KhuVuc = new KhuVuc();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
                KhuVuc.Ten = textInfo.ToTitleCase(txtArea.Text.Trim().ToLower());
                KhuVuc.InsertUpdate();
                ShowMessage("Lưu thông tin thành công", false, false);
                this.Close();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            isValid &= ValidateControl.ValidateNull(txtArea, errorProvider, "Khu vực", isOnlyWarning);
            return isValid;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
