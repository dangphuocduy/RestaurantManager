﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

namespace RestaurantManager
{
    public partial class TableAndRoomForm : BaseForm
    {
        public PhongBan PhongBan;
        public TableAndRoomForm()
        {
            InitializeComponent();
        }

        private void TableAndRoomForm_Load(object sender, EventArgs e)
        {
            try
            {
                LoadKhuVuc();
                if (PhongBan == null)
                {
                    PhongBan = new PhongBan();
                }
                else
                {
                    SetData();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void LoadKhuVuc()
        {
            try
            {
                cbbKhuVuc.DataSource = KhuVuc.SelectAll().Tables[0];
                cbbKhuVuc.DisplayMember = "Ten";
                cbbKhuVuc.ValueMember = "Id";
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void GetData()
        {
            try
            {
                TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
                PhongBan.TenPhongBan = textInfo.ToTitleCase(txtTenPhongBan.Text.Trim().ToLower());
                PhongBan.KhuVucId = Convert.ToInt32(cbbKhuVuc.SelectedValue);
                PhongBan.STT = Convert.ToInt32(txtSoThuTu.Text.Trim());
                PhongBan.SoGhe = Convert.ToInt32(txtSoGhe.Text.Trim());
                PhongBan.GhiChu = textInfo.ToTitleCase(txtGhiChu.Text.Trim().ToLower());
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void SetData()
        {
            try
            {
                txtTenPhongBan.Text = PhongBan.TenPhongBan;
                cbbKhuVuc.SelectedValue = PhongBan.KhuVucId;
                txtSoThuTu.Text = PhongBan.STT.ToString();
                txtSoGhe.Text = PhongBan.SoGhe.ToString();
                txtGhiChu.Text = PhongBan.GhiChu;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            isValid &= ValidateControl.ValidateNull(txtTenPhongBan, errorProvider, "Tên phòng bàn", isOnlyWarning);
            isValid &= ValidateControl.ValidateChoose(cbbKhuVuc, errorProvider, "Khu vực", isOnlyWarning);
            return isValid;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                GetData();
                PhongBan.InsertUpdate();
                ShowMessage("Lưu thông tin thành công", false, false);
                this.Close();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
