﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace RestaurantManager
{
	public partial class USER_GROUP : ICloneable
	{
		#region Properties.
		
		public long GROUPS_ID { set; get; }
		public long USER_ID { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<USER_GROUP> ConvertToCollection(IDataReader reader)
		{
			List<USER_GROUP> collection = new List<USER_GROUP>();
			while (reader.Read())
			{
				USER_GROUP entity = new USER_GROUP();
				if (!reader.IsDBNull(reader.GetOrdinal("GROUPS_ID"))) entity.GROUPS_ID = reader.GetInt64(reader.GetOrdinal("GROUPS_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("USER_ID"))) entity.USER_ID = reader.GetInt64(reader.GetOrdinal("USER_ID"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO USER_GROUP VALUES(@GROUPS_ID, @USER_ID)";
            string update = "UPDATE USER_GROUP SET GROUPS_ID = @GROUPS_ID, USER_ID = @USER_ID WHERE ID = @ID";
            string delete = "DELETE FROM USER_GROUP WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@GROUPS_ID", SqlDbType.BigInt, "GROUPS_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@USER_ID", SqlDbType.BigInt, "USER_ID", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@GROUPS_ID", SqlDbType.BigInt, "GROUPS_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@USER_ID", SqlDbType.BigInt, "USER_ID", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@GROUPS_ID", SqlDbType.BigInt, "GROUPS_ID", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@USER_ID", SqlDbType.BigInt, "USER_ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO USER_GROUP VALUES(@GROUPS_ID, @USER_ID)";
            string update = "UPDATE USER_GROUP SET GROUPS_ID = @GROUPS_ID, USER_ID = @USER_ID WHERE ID = @ID";
            string delete = "DELETE FROM USER_GROUP WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@GROUPS_ID", SqlDbType.BigInt, "GROUPS_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@USER_ID", SqlDbType.BigInt, "USER_ID", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@GROUPS_ID", SqlDbType.BigInt, "GROUPS_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@USER_ID", SqlDbType.BigInt, "USER_ID", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@GROUPS_ID", SqlDbType.BigInt, "GROUPS_ID", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@USER_ID", SqlDbType.BigInt, "USER_ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static USER_GROUP Load(long gROUPS_ID, long uSER_ID)
		{
			const string spName = "[dbo].[p_USER_GROUP_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@GROUPS_ID", SqlDbType.BigInt, gROUPS_ID);
			db.AddInParameter(dbCommand, "@USER_ID", SqlDbType.BigInt, uSER_ID);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<USER_GROUP> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_USER_GROUP_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_USER_GROUP_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_USER_GROUP_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_USER_GROUP_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertUSER_GROUP(long gROUPS_ID, long uSER_ID)
		{
			USER_GROUP entity = new USER_GROUP();	
			entity.GROUPS_ID = gROUPS_ID;
			entity.USER_ID = uSER_ID;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_USER_GROUP_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@GROUPS_ID", SqlDbType.BigInt, GROUPS_ID);
			db.AddInParameter(dbCommand, "@USER_ID", SqlDbType.BigInt, USER_ID);
			
			if (transaction != null)
			{
				return db.ExecuteNonQuery(dbCommand, transaction);
			}
            else
			{
				return db.ExecuteNonQuery(dbCommand);
			}			
		}
				
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateUSER_GROUP(long gROUPS_ID, long uSER_ID)
		{
			USER_GROUP entity = new USER_GROUP();			
			entity.GROUPS_ID = gROUPS_ID;
			entity.USER_ID = uSER_ID;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_USER_GROUP_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@GROUPS_ID", SqlDbType.BigInt, GROUPS_ID);
			db.AddInParameter(dbCommand, "@USER_ID", SqlDbType.BigInt, USER_ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateUSER_GROUP(long gROUPS_ID, long uSER_ID)
		{
			USER_GROUP entity = new USER_GROUP();			
			entity.GROUPS_ID = gROUPS_ID;
			entity.USER_ID = uSER_ID;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_USER_GROUP_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@GROUPS_ID", SqlDbType.BigInt, GROUPS_ID);
			db.AddInParameter(dbCommand, "@USER_ID", SqlDbType.BigInt, USER_ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteUSER_GROUP(long gROUPS_ID, long uSER_ID)
		{
			USER_GROUP entity = new USER_GROUP();
			entity.GROUPS_ID = gROUPS_ID;
			entity.USER_ID = uSER_ID;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_USER_GROUP_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@GROUPS_ID", SqlDbType.BigInt, GROUPS_ID);
			db.AddInParameter(dbCommand, "@USER_ID", SqlDbType.BigInt, USER_ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_USER_GROUP_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}