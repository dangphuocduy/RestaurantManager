﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using System.Globalization;

namespace RestaurantManager
{
    public partial class CashierForm : BaseForm
    {
        public string where;
        public PhongBan PhongBan;
        public PhongBan_HangHoa PhongBan_HangHoa;
        public CashierForm()
        {
            InitializeComponent();
        }

        private void CashierForm_Load(object sender, EventArgs e)
        {
            LoadProducts();
            LoadTableAndRoom();
            LoadCategoty();
            LoadKhuVuc();
            cbbStatus.SelectedValue = 0;
        }

        private string GetSearchWhere()
        {
            try
            {
                where = " 1 = 1";
                if (!String.IsNullOrEmpty(txtTenHangHoa.Text))
                {
                    where += " AND TenHangHoa LIKE N'%" + txtTenHangHoa.Text + "%' OR MaHangHoa LIKE N'%" + txtTenHangHoa.Text + "%'";
                }
                if (cbbCategory.SelectedValue != null)
                {
                    where += " AND NhomHangHoaId = '" + cbbCategory.SelectedValue + "'";
                }
                return where;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                dgListProducts.Refetch();
                dgListProducts.DataSource = HangHoa.SelectDynamic(GetSearchWhere(), null).Tables[0];
                dgListProducts.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void LoadProducts()
        {
            try
            {
                dgListProducts.Refetch();
                dgListProducts.DataSource = HangHoa.SelectAll().Tables[0];
                dgListProducts.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void LoadTableAndRoom()
        {
            try
            {
                dgListTableAndRoom.Refetch();
                dgListTableAndRoom.DataSource = PhongBan.SelectAll().Tables[0];
                dgListTableAndRoom.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void LoadKhuVuc()
        {
            try
            {
                cbbKhuVuc.DataSource = KhuVuc.SelectAll().Tables[0];
                cbbKhuVuc.DisplayMember = "Ten";
                cbbKhuVuc.ValueMember = "Id";
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void LoadCategoty()
        {
            try
            {
                cbbCategory.DataSource = NhomHangHoa.SelectAll().Tables[0];
                cbbCategory.DisplayMember = "TenNhom";
                cbbCategory.ValueMember = "Id";
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgList_SelectionChanged(object sender, EventArgs e)
        {
            //try
            //{
            //    if (dgList.CurrentRow.RowType == RowType.Record)
            //    {
            //        int id = System.Convert.ToInt32(dgList.CurrentRow.Cells["Id"].Value.ToString());
            //        PhongBan_HangHoa = PhongBan_HangHoa.Load(id);
            //        txtSoLuong.Text = PhongBan_HangHoa.SoLuong.ToString();
            //        txtDonGia.Text = PhongBan_HangHoa.DonGia.ToString();
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Logger.LocalLogger.Instance().WriteMessage(ex);
            //}
        }
        private void SetProducts()
        {
            try
            {
                dgList.Refresh();
                dgList.DataSource = PhongBan.HangHoaCollection;
                dgList.Refetch();
                if (PhongBan.HangHoaCollection.Count > 0)
                {
                    decimal ToTalMoney = 0;
                    foreach (var item in PhongBan.HangHoaCollection)
                    {
                        ToTalMoney += item.ThanhTien;
                    }
                    CultureInfo cultureInfo = CultureInfo.GetCultureInfo("vi-VN");
                    string TongTien = decimal.Parse(ToTalMoney.ToString()).ToString("#,###", cultureInfo.NumberFormat);
                    lblTongTien.Text = TongTien;
                }
                else
                {
                    lblTongTien.Text = "0";
                }
                lblTableName.Text = PhongBan.TenPhongBan;
                txtKhachHang.Text = PhongBan.KhachHang;
                lblThoiGianTao.Text = PhongBan.ThoiGianTao == DateTime.MinValue ? DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss") : PhongBan.ThoiGianTao.ToString("dd-MM-yyyy HH:mm:ss");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgListTableAndRoom_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgListTableAndRoom.CurrentRow.RowType == RowType.Record)
                {
                    int id = System.Convert.ToInt32(dgListTableAndRoom.CurrentRow.Cells["Id"].Value.ToString());
                    PhongBan = PhongBan.Load(id);
                    PhongBan.HangHoaCollection = PhongBan_HangHoa.SelectCollectionBy_PhongBanId(PhongBan.Id);
                    SetProducts();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgListProducts_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgListProducts.CurrentRow.RowType == RowType.Record)
                {
                    if (PhongBan != null)
                    {
                        int id = System.Convert.ToInt32(dgListProducts.CurrentRow.Cells["Id"].Value.ToString());
                        HangHoa HangHoa = HangHoa.Load(id);
                        PhongBan_HangHoa PhongBan_HangHoa = new PhongBan_HangHoa();
                        PhongBan_HangHoa.MaHangHoa = HangHoa.MaHangHoa;
                        PhongBan_HangHoa.TenHangHoa = HangHoa.TenHangHoa;
                        PhongBan_HangHoa.SoLuong = 1;
                        PhongBan_HangHoa.LoaiThucDonId = HangHoa.LoaiThucDonId;
                        PhongBan_HangHoa.NhomHangHoaId = HangHoa.NhomHangHoaId;
                        PhongBan_HangHoa.DonGia = HangHoa.DonGia;
                        PhongBan_HangHoa.DonViTinh = HangHoa.DonViTinh;
                        PhongBan_HangHoa.ThanhTien = PhongBan_HangHoa.DonGia * PhongBan_HangHoa.SoLuong;
                        bool isAddNew = true;
                        if (PhongBan.HangHoaCollection.Count == 0)
                        {
                            PhongBan.ThoiGianTao = DateTime.Now;
                        }
                        foreach (var item in PhongBan.HangHoaCollection)
                        {
                            if (PhongBan_HangHoa.MaHangHoa == item.MaHangHoa)
                            {
                                item.SoLuong += 1;
                                item.ThanhTien = item.SoLuong * item.DonGia;
                                isAddNew = false;
                            }
                        }

                        if (isAddNew)
                        {
                            PhongBan.HangHoaCollection.Add(PhongBan_HangHoa);
                        }

                        PhongBan.InsertUpdateFull();
                        SetProducts();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgListProducts_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    if (PhongBan != null)
                    {
                        long id = Convert.ToInt64(e.Row.Cells["Id"].Value);
                        HangHoa HangHoa = HangHoa.Load(id);
                        PhongBan_HangHoa PhongBan_HangHoa = new PhongBan_HangHoa();
                        PhongBan_HangHoa.MaHangHoa = HangHoa.MaHangHoa;
                        PhongBan_HangHoa.TenHangHoa = HangHoa.TenHangHoa;
                        PhongBan_HangHoa.SoLuong = 1;
                        PhongBan_HangHoa.LoaiThucDonId = HangHoa.LoaiThucDonId;
                        PhongBan_HangHoa.NhomHangHoaId = HangHoa.NhomHangHoaId;
                        PhongBan_HangHoa.DonGia = HangHoa.DonGia;
                        PhongBan_HangHoa.DonViTinh = HangHoa.DonViTinh;
                        PhongBan_HangHoa.ThanhTien = PhongBan_HangHoa.DonGia * PhongBan_HangHoa.SoLuong;
                        bool isAddNew = true;
                        if (PhongBan.HangHoaCollection.Count == 0)
                        {
                            PhongBan.ThoiGianTao = DateTime.Now;
                        }
                        foreach (var item in PhongBan.HangHoaCollection)
                        {
                            if (PhongBan_HangHoa.MaHangHoa == item.MaHangHoa)
                            {
                                item.SoLuong += 1;
                                item.ThanhTien = item.SoLuong * item.DonGia;
                                isAddNew = false;
                            }
                        }
                        if (isAddNew)
                        {
                            PhongBan.HangHoaCollection.Add(PhongBan_HangHoa);
                        }

                        PhongBan.InsertUpdateFull();
                        SetProducts();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                List<PhongBan_HangHoa> ItemColl = new List<PhongBan_HangHoa>();
                if (dgList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn có chắc chắn muốn xóa hàng hoá này không?", true, false) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((PhongBan_HangHoa)i.GetRow().DataRow);
                        }

                    }
                    foreach (PhongBan_HangHoa item in ItemColl)
                    {
                        if (item.Id > 0)
                            item.Delete();
                        PhongBan.HangHoaCollection.Remove(item);
                    }
                    ShowMessage("Xóa thành công. ", false, false);
                    SetProducts();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }


            //try
            //{
            //    if (dgList.CurrentRow.RowType == RowType.Record)
            //    {
            //        int id = System.Convert.ToInt32(dgList.CurrentRow.Cells["Id"].Value.ToString());
            //        if (ShowMessage("Bạn có chắc chắn muốn xóa hàng hoá này không?", true, false) == "Yes")
            //        {
            //            PhongBan_HangHoa HangHoa = PhongBan_HangHoa.Load(id);
            //            HangHoa.Delete();
            //            PhongBan_HangHoa deletedItem = new PhongBan_HangHoa();
            //            foreach (var item in PhongBan.HangHoaCollection)
            //            {

            //                if (HangHoa.MaHangHoa == item.MaHangHoa)
            //                {
            //                    deletedItem = item;
            //                }
            //            }
            //            PhongBan.HangHoaCollection.Remove(deletedItem);
            //            ShowMessage("Xóa thành công. ", false, false);
            //            SetProducts();
            //        }
            //        else
            //            ShowMessage("Xóa không thành công. ", false, false);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Logger.LocalLogger.Instance().WriteMessage(ex);
            //}
        }

        private string GetSearchTableWhere()
        {
            try
            {
                where = " 1 = 1";
                if (!String.IsNullOrEmpty(txtTable.Text))
                {
                    where += " AND TenPhongBan LIKE N'%" + txtTable.Text + "%'";
                }
                if (cbbKhuVuc.SelectedValue != null)
                {
                    where += " AND KhuVucId = '" + cbbKhuVuc.SelectedValue + "'";
                }
                return where;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }

        private void txtTable_TextChanged(object sender, EventArgs e)
        {
            try
            {
                //DataTable dataTable = PhongBan.SelectDynamic(GetSearchTableWhere(), null).Tables[0];
                List<PhongBan> PhongBanCollection = PhongBan.SelectCollectionDynamic(GetSearchTableWhere(), null);
                //Helpers helpers = new Helpers();
                //List<PhongBan> PhongBanCollection = new List<PhongBan>();
                //PhongBanCollection = helpers.ConvertDataTable<PhongBan>(dataTable);
                List<PhongBan> PhongBanCollectionSelect = new List<PhongBan>();
                foreach (var item in PhongBanCollection)
                {
                    item.HangHoaCollection = PhongBan_HangHoa.SelectCollectionBy_PhongBanId(item.Id);
                }

                if (cbbStatus.SelectedValue.ToString() == "1")
                {
                    PhongBanCollectionSelect = PhongBanCollection.Where(x => x.HangHoaCollection.Count() > 0).ToList<PhongBan>();
                }
                else if (cbbStatus.SelectedValue.ToString() == "2")
                {
                    PhongBanCollectionSelect = PhongBanCollection.Where(x => x.HangHoaCollection.Count() == 0).ToList<PhongBan>();
                }
                else
                {
                    PhongBanCollectionSelect = PhongBanCollection;
                }

                dgListTableAndRoom.Refetch();
                dgListTableAndRoom.DataSource = PhongBanCollectionSelect;//PhongBan.SelectDynamic(GetSearchTableWhere(), null).Tables[0];
                dgListTableAndRoom.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private string GetMaHoaDon()
        {
            try
            {
                string MaHoaDon = string.Empty;
                List<HoaDon> HoaDonCollection = HoaDon.SelectCollectionAll();
                if (HoaDonCollection.Count == 0)
                {
                    MaHoaDon = "HD0000000001";
                }
                else
                {
                    var LastHoaDon = HoaDonCollection.Last();
                    var NumberHoaDon = Convert.ToInt32(LastHoaDon.MaHoaDon.Substring(2, 10));
                    if (NumberHoaDon >= 0 && NumberHoaDon < 9)
                    {
                        MaHoaDon = "HD000000000" + (NumberHoaDon + 1);
                    }
                    else if (NumberHoaDon >= 9 && NumberHoaDon < 99)
                    {
                        MaHoaDon = "HD00000000" + (NumberHoaDon + 1);
                    }
                    else if (NumberHoaDon >= 99 && NumberHoaDon < 999)
                    {
                        MaHoaDon = "HD0000000" + (NumberHoaDon + 1);
                    }
                    else if (NumberHoaDon >= 999 && NumberHoaDon < 9999)
                    {
                        MaHoaDon = "HD000000" + (NumberHoaDon + 1);
                    }
                    else if (NumberHoaDon >= 9999 && NumberHoaDon < 99999)
                    {
                        MaHoaDon = "HD00000" + (NumberHoaDon + 1);
                    }
                    else if (NumberHoaDon >= 99999 && NumberHoaDon < 999999)
                    {
                        MaHoaDon = "HD0000" + (NumberHoaDon + 1);
                    }
                    else if (NumberHoaDon >= 999999 && NumberHoaDon < 9999999)
                    {
                        MaHoaDon = "HD000" + (NumberHoaDon + 1);
                    }
                    else if (NumberHoaDon >= 9999999 && NumberHoaDon < 99999999)
                    {
                        MaHoaDon = "HD00" + (NumberHoaDon + 1);
                    }
                    else if (NumberHoaDon >= 99999999 && NumberHoaDon < 999999999)
                    {
                        MaHoaDon = "HD0" + (NumberHoaDon + 1);
                    }
                    else if (NumberHoaDon >= 999999999 && NumberHoaDon < 9999999999)
                    {
                        MaHoaDon = "HD" + (NumberHoaDon + 1);
                    }
                }
                return MaHoaDon;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        private void btnCash_Click(object sender, EventArgs e)
        {
            try
            {
                if (PhongBan.HangHoaCollection.Count() > 0)
                {
                    if (ShowMessage("Bạn có chắc chắn muốn thanh toán phòng bàn " + txtTable.Text + " này không?", true, false) == "Yes")
                    {
                        HoaDon HoaDon = new HoaDon();
                        HoaDon.MaHoaDon = GetMaHoaDon();
                        HoaDon.PhongBan = PhongBan.TenPhongBan.ToString();
                        HoaDon.ThoiGianTao = PhongBan.ThoiGianTao == DateTime.MinValue ? DateTime.Now : PhongBan.ThoiGianTao;
                        HoaDon.ThoiGianThanhToan = DateTime.Now;
                        HoaDon.TongTien = Convert.ToDecimal(lblTongTien.Text);
                        HoaDon.ThueSuat = Convert.ToInt32(txtThueGTGT.Text);
                        int i = 1;
                        HoaDon.HangHoaCollection = new List<HoaDon_HangHoa>();
                        foreach (var item in PhongBan.HangHoaCollection)
                        {
                            HoaDon_HangHoa hanghoa = new HoaDon_HangHoa();
                            hanghoa.STT = i;
                            hanghoa.MaHangHoa = item.MaHangHoa;
                            hanghoa.TenHangHoa = item.TenHangHoa;
                            hanghoa.LoaiThucDonId = item.LoaiThucDonId;
                            hanghoa.NhomHangHoaId = item.NhomHangHoaId;
                            hanghoa.SoLuong = item.SoLuong;
                            hanghoa.DonGia = item.DonGia;
                            hanghoa.DonViTinh = item.DonViTinh;
                            hanghoa.ThanhTien = item.ThanhTien;
                            i++;
                            HoaDon.HangHoaCollection.Add(hanghoa);
                        }
                        HoaDon.InsertUpdateFull();
                        foreach (var item in PhongBan.HangHoaCollection)
                        {
                            item.Delete();
                        }
                        PhongBan = new PhongBan();
                        SetProducts();
                        LoadTableAndRoom();
                        ShowMessage("Thanh toán thành công. ", false, false);
                        Helpers helper = new Helpers();
                        helper.SendEmmail(HoaDon);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                if (PhongBan != null)
                {
                    if (PhongBan.HangHoaCollection.Count() > 0)
                    {
                        int ThueGTGT = 0;
                        if (!String.IsNullOrEmpty(txtThueGTGT.Text))
                        {
                            ThueGTGT = Convert.ToInt32(txtThueGTGT.Text);
                        }
                        K80PrintTemplates f = new K80PrintTemplates();
                        f.PhongBan = PhongBan;
                        f.ThueGTGT = ThueGTGT;
                        f.BindReport();
                        if (GlobalSettings.PREVIEW)
                        {
                            f.ShowPreviewDialog();
                        }
                        else
                        {
                            f.Print();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    decimal SoLuong = (decimal)e.Row.Cells["SoLuong"].Value;
                    decimal DonGia = (decimal)e.Row.Cells["DonGia"].Value;
                    decimal ThanhTien = (decimal)e.Row.Cells["ThanhTien"].Value;
                    e.Row.Cells["SoLuong"].Text = ToTrimmedString(SoLuong);
                    e.Row.Cells["DonGia"].Text = DonGia.ToString("#,##0");
                    e.Row.Cells["ThanhTien"].Text = ThanhTien.ToString("#,##0");
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public string ToTrimmedString(decimal num)
        {
            try
            {
                string str = num.ToString();
                string decimalSeparator = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
                if (str.Contains(decimalSeparator))
                {
                    str = str.TrimEnd('0');
                    if (str.EndsWith(decimalSeparator))
                    {
                        str = str.Remove(str.Length - 1, 1);
                    }
                }
                return str;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return null;
        }

        public string RemoveFromEnd(string str, int characterCount)
        {
            try
            {
                return str.Remove(str.Length - characterCount, characterCount);

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        private void dgListProducts_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    decimal DonGia = (decimal)e.Row.Cells["DonGia"].Value;
                    e.Row.Cells["DonGia"].Text = DonGia.ToString("#,##0");
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtKhachHang_TextChanged(object sender, EventArgs e)
        {
            try
            {
                PhongBan.KhachHang = txtKhachHang.Text.Trim();
                PhongBan.Update();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgList_CellValueChanged(object sender, ColumnActionEventArgs e)
        {
            try
            {
                dgList.Update();
                string value = string.Empty;
                if (e.Column.DataMember == "SoLuong")
                {
                    if (dgList.CurrentRow.Cells["SoLuong"].Value != null)
                    {
                        var a = dgList.CurrentRow.Cells["SoLuong"].Value;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgListTableAndRoom_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    long KhuVucIdId = (long)e.Row.Cells["KhuVucId"].Value;
                    e.Row.Cells["KhuVucId"].Text = KhuVuc.Load(KhuVucIdId).Ten;

                    long Id = (long)e.Row.Cells["Id"].Value;
                    PhongBan PhongBan = PhongBan.Load(Id);
                    PhongBan.HangHoaCollection = PhongBan_HangHoa.SelectCollectionBy_PhongBanId(PhongBan.Id);
                    if (PhongBan.HangHoaCollection.Count > 0)
                    {
                        Janus.Windows.GridEX.GridEXFormatStyle style = new Janus.Windows.GridEX.GridEXFormatStyle();
                        style.BackColor = Color.LightSeaGreen;
                        style.ForeColor = Color.White;
                        e.Row.RowStyle = style;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    if (PhongBan != null)
                    {
                        long id = Convert.ToInt64(e.Row.Cells["Id"].Value);
                        PhongBan_HangHoa = PhongBan_HangHoa.Load(id);
                        txtSoLuong.Text = PhongBan_HangHoa.SoLuong.ToString();
                        txtDonGia.Text = PhongBan_HangHoa.DonGia.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            isValid &= ValidateControl.ValidateNull(txtSoLuong, errorProvider, "Số lượng", isOnlyWarning);
            isValid &= ValidateControl.ValidateNull(txtDonGia, errorProvider, "Đơn giá", isOnlyWarning);
            return isValid;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {

                if (!ValidateForm(false))
                    return;
                if (PhongBan_HangHoa != null)
                {
                    PhongBan_HangHoa.SoLuong = Convert.ToDecimal(txtSoLuong.Text);
                    PhongBan_HangHoa.DonGia = Convert.ToDecimal(txtDonGia.Text.Replace(" ₫", ""));
                    PhongBan_HangHoa.ThanhTien = PhongBan_HangHoa.SoLuong * PhongBan_HangHoa.DonGia;
                    PhongBan_HangHoa.Update();
                    PhongBan.HangHoaCollection = PhongBan_HangHoa.SelectCollectionBy_PhongBanId(PhongBan.Id);
                    SetProducts();
                    txtSoLuong.Text = String.Empty;
                    txtDonGia.Text = String.Empty;
                    PhongBan_HangHoa = null;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtSoLuong_TextChanged(object sender, EventArgs e)
        {
            try
            {
                //txtSoLuong.Text = string.Format("{0:#.##}",txtSoLuong.Text);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtThueGTGT_TextChanged(object sender, EventArgs e)
        {
            try
            {
                decimal ToTalMoney = 0;
                foreach (var item in PhongBan.HangHoaCollection)
                {
                    ToTalMoney += item.ThanhTien;
                }
                decimal Tax;
                if (!String.IsNullOrEmpty(txtThueGTGT.Text.Trim()))
                {
                    if (txtThueGTGT.Text.Trim() != "0")
                    {
                        Tax = ToTalMoney * Convert.ToInt32(txtThueGTGT.Text) / 100;
                        ToTalMoney = ToTalMoney + Tax;
                        CultureInfo cultureInfo = CultureInfo.GetCultureInfo("vi-VN");
                        string TongTien = decimal.Parse(ToTalMoney.ToString()).ToString("#,###", cultureInfo.NumberFormat);
                        lblTongTien.Text = TongTien;
                    }
                    else
                    {
                        CultureInfo cultureInfo = CultureInfo.GetCultureInfo("vi-VN");
                        string TongTien = decimal.Parse(ToTalMoney.ToString()).ToString("#,###", cultureInfo.NumberFormat);
                        lblTongTien.Text = TongTien;

                    }
                }
                else
                {
                    CultureInfo cultureInfo = CultureInfo.GetCultureInfo("vi-VN");
                    string TongTien = decimal.Parse(ToTalMoney.ToString()).ToString("#,###", cultureInfo.NumberFormat);
                    lblTongTien.Text = TongTien;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgList_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgList.CurrentRow.RowType == RowType.Record)
                {
                    int id = System.Convert.ToInt32(dgList.CurrentRow.Cells["Id"].Value.ToString());
                    PhongBan_HangHoa = PhongBan_HangHoa.Load(id);
                    txtSoLuong.Text = PhongBan_HangHoa.SoLuong.ToString();
                    txtDonGia.Text = PhongBan_HangHoa.DonGia.ToString();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgListProducts_Click(object sender, EventArgs e)
        {

        }

        private void btnSplitTable_Click(object sender, EventArgs e)
        {
            try
            {
                SplitAndMergeTableForm f = new SplitAndMergeTableForm();
                if (PhongBan.HangHoaCollection.Count == 0)
                {
                    ShowMessage("Danh sách hàng hoá trống !",false);
                    return;
                }
                f.PhongBanSource = PhongBan;
                f.ShowDialog(this);
                txtTable_TextChanged(null,null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
