﻿namespace RestaurantManager
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.Common.JanusColorScheme janusColorScheme1 = new Janus.Windows.Common.JanusColorScheme();
            Janus.Windows.Common.JanusColorScheme janusColorScheme2 = new Janus.Windows.Common.JanusColorScheme();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup1 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem1 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup2 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem2 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem3 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup3 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem4 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem5 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup4 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem6 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem7 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup5 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem8 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem9 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup6 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem10 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup7 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem11 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem12 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem13 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            this.vsMain = new Janus.Windows.Common.VisualStyleManager(this.components);
            this.pnMain = new Janus.Windows.UI.Dock.UIPanelManager(this.components);
            this.grPnSystem = new Janus.Windows.UI.Dock.UIPanelGroup();
            this.pnCashier = new Janus.Windows.UI.Dock.UIPanel();
            this.pnCashierContainer = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.explorerBarCashier = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.pnTableAndRoom = new Janus.Windows.UI.Dock.UIPanel();
            this.pnClassRoomContainer = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.explorerBarTableAndRoom = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.pnProducts = new Janus.Windows.UI.Dock.UIPanel();
            this.pnCategoryContainer = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.explorerBarProducts = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.pnCashFlow = new Janus.Windows.UI.Dock.UIPanel();
            this.pnCashFlowContainer = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.explorerBarCashFlow = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.cmdMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdSystem1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSystem");
            this.cmdBackupDatabase = new Janus.Windows.UI.CommandBars.UICommand("cmdBackupDatabase");
            this.cmdConfigConnection = new Janus.Windows.UI.CommandBars.UICommand("cmdConfigConnection");
            this.cmdExit = new Janus.Windows.UI.CommandBars.UICommand("cmdExit");
            this.cmdSQLQuery = new Janus.Windows.UI.CommandBars.UICommand("cmdSQLQuery");
            this.cmdLog = new Janus.Windows.UI.CommandBars.UICommand("cmdLog");
            this.cmdConfigSystem = new Janus.Windows.UI.CommandBars.UICommand("cmdConfigSystem");
            this.cmdSystem = new Janus.Windows.UI.CommandBars.UICommand("cmdSystem");
            this.cmdConfigSystem1 = new Janus.Windows.UI.CommandBars.UICommand("cmdConfigSystem");
            this.cmdConfigConnection1 = new Janus.Windows.UI.CommandBars.UICommand("cmdConfigConnection");
            this.cmdBackupDatabase1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBackupDatabase");
            this.cmdSQLQuery1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSQLQuery");
            this.cmdLog1 = new Janus.Windows.UI.CommandBars.UICommand("cmdLog");
            this.cmdLoginUser1 = new Janus.Windows.UI.CommandBars.UICommand("cmdLoginUser");
            this.cmdChangePassword1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChangePassword");
            this.cmdUser1 = new Janus.Windows.UI.CommandBars.UICommand("cmdUser");
            this.cmdExit1 = new Janus.Windows.UI.CommandBars.UICommand("cmdExit");
            this.cmdLoginUser = new Janus.Windows.UI.CommandBars.UICommand("cmdLoginUser");
            this.cmdChangePassword = new Janus.Windows.UI.CommandBars.UICommand("cmdChangePassword");
            this.cmdUser = new Janus.Windows.UI.CommandBars.UICommand("cmdUser");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.grbMain = new Janus.Windows.EditControls.UIGroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.pnMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grPnSystem)).BeginInit();
            this.grPnSystem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnCashier)).BeginInit();
            this.pnCashier.SuspendLayout();
            this.pnCashierContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.explorerBarCashier)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnTableAndRoom)).BeginInit();
            this.pnTableAndRoom.SuspendLayout();
            this.pnClassRoomContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.explorerBarTableAndRoom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnProducts)).BeginInit();
            this.pnProducts.SuspendLayout();
            this.pnCategoryContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.explorerBarProducts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnCashFlow)).BeginInit();
            this.pnCashFlow.SuspendLayout();
            this.pnCashFlowContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.explorerBarCashFlow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.SuspendLayout();
            // 
            // vsMain
            // 
            janusColorScheme1.HighlightTextColor = System.Drawing.SystemColors.HighlightText;
            janusColorScheme1.Name = "Office2003";
            janusColorScheme1.Office2007CustomColor = System.Drawing.Color.Empty;
            janusColorScheme2.HighlightTextColor = System.Drawing.SystemColors.HighlightText;
            janusColorScheme2.InfoTextColor = System.Drawing.Color.White;
            janusColorScheme2.MenuTextColor = System.Drawing.Color.White;
            janusColorScheme2.Name = "Office2007";
            janusColorScheme2.Office2007CustomColor = System.Drawing.Color.Empty;
            janusColorScheme2.UseThemes = false;
            janusColorScheme2.VisualStyle = Janus.Windows.Common.VisualStyle.Office2007;
            this.vsMain.ColorSchemes.Add(janusColorScheme1);
            this.vsMain.ColorSchemes.Add(janusColorScheme2);
            this.vsMain.DefaultColorScheme = "Office2007";
            // 
            // pnMain
            // 
            this.pnMain.ContainerControl = this;
            this.pnMain.DefaultPanelSettings.CaptionHeight = 30;
            this.pnMain.DefaultPanelSettings.CaptionStyle = Janus.Windows.UI.Dock.PanelCaptionStyle.Dark;
            this.pnMain.DefaultPanelSettings.InnerAreaStyle = Janus.Windows.UI.Dock.PanelInnerAreaStyle.Window;
            this.pnMain.DefaultTabGroupStyle = Janus.Windows.UI.Dock.TabGroupStyle.OutlookNavigator;
            this.pnMain.TabbedMdi = true;
            this.pnMain.VisualStyleManager = this.vsMain;
            this.grPnSystem.Id = new System.Guid("270d300a-7d1e-4752-bbc0-de361bda642e");
            this.grPnSystem.StaticGroup = true;
            this.pnCashier.Id = new System.Guid("3783e0ec-4cf0-4ead-9214-ee11f3cb9b08");
            this.grPnSystem.Panels.Add(this.pnCashier);
            this.pnTableAndRoom.Id = new System.Guid("66d13b7d-b7c8-4703-8642-c53ec987dd12");
            this.grPnSystem.Panels.Add(this.pnTableAndRoom);
            this.pnProducts.Id = new System.Guid("87b88725-41a8-4ec6-9ac3-9eb57b920ca6");
            this.grPnSystem.Panels.Add(this.pnProducts);
            this.pnCashFlow.Id = new System.Guid("997c8f71-948f-4e45-9e97-f7b620756328");
            this.grPnSystem.Panels.Add(this.pnCashFlow);
            this.pnMain.Panels.Add(this.grPnSystem);
            // 
            // Design Time Panel Info:
            // 
            this.pnMain.BeginPanelInfo();
            this.pnMain.AddDockPanelInfo(new System.Guid("270d300a-7d1e-4752-bbc0-de361bda642e"), Janus.Windows.UI.Dock.PanelGroupStyle.OutlookNavigator, Janus.Windows.UI.Dock.PanelDockStyle.Left, true, new System.Drawing.Size(252, 479), true);
            this.pnMain.AddDockPanelInfo(new System.Guid("3783e0ec-4cf0-4ead-9214-ee11f3cb9b08"), new System.Guid("270d300a-7d1e-4752-bbc0-de361bda642e"), -1, true);
            this.pnMain.AddDockPanelInfo(new System.Guid("66d13b7d-b7c8-4703-8642-c53ec987dd12"), new System.Guid("270d300a-7d1e-4752-bbc0-de361bda642e"), -1, true);
            this.pnMain.AddDockPanelInfo(new System.Guid("87b88725-41a8-4ec6-9ac3-9eb57b920ca6"), new System.Guid("270d300a-7d1e-4752-bbc0-de361bda642e"), -1, true);
            this.pnMain.AddDockPanelInfo(new System.Guid("997c8f71-948f-4e45-9e97-f7b620756328"), new System.Guid("270d300a-7d1e-4752-bbc0-de361bda642e"), -1, true);
            this.pnMain.AddFloatingPanelInfo(new System.Guid("270d300a-7d1e-4752-bbc0-de361bda642e"), Janus.Windows.UI.Dock.PanelGroupStyle.OutlookNavigator, true, new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pnMain.AddFloatingPanelInfo(new System.Guid("66d13b7d-b7c8-4703-8642-c53ec987dd12"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pnMain.AddFloatingPanelInfo(new System.Guid("87b88725-41a8-4ec6-9ac3-9eb57b920ca6"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pnMain.AddFloatingPanelInfo(new System.Guid("997c8f71-948f-4e45-9e97-f7b620756328"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pnMain.AddFloatingPanelInfo(new System.Guid("3783e0ec-4cf0-4ead-9214-ee11f3cb9b08"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pnMain.EndPanelInfo();
            // 
            // grPnSystem
            // 
            this.grPnSystem.GroupStyle = Janus.Windows.UI.Dock.PanelGroupStyle.OutlookNavigator;
            this.grPnSystem.Location = new System.Drawing.Point(3, 33);
            this.grPnSystem.Name = "grPnSystem";
            this.grPnSystem.SelectedPanel = this.pnCashier;
            this.grPnSystem.Size = new System.Drawing.Size(252, 479);
            this.grPnSystem.TabIndex = 4;
            this.grPnSystem.Text = "Nhà hàng";
            // 
            // pnCashier
            // 
            this.pnCashier.Image = ((System.Drawing.Image)(resources.GetObject("pnCashier.Image")));
            this.pnCashier.InnerContainer = this.pnCashierContainer;
            this.pnCashier.Location = new System.Drawing.Point(0, 0);
            this.pnCashier.Name = "pnCashier";
            this.pnCashier.Size = new System.Drawing.Size(248, 311);
            this.pnCashier.TabIndex = 4;
            this.pnCashier.Text = "Thu Ngân";
            // 
            // pnCashierContainer
            // 
            this.pnCashierContainer.Controls.Add(this.explorerBarCashier);
            this.pnCashierContainer.Location = new System.Drawing.Point(1, 31);
            this.pnCashierContainer.Name = "pnCashierContainer";
            this.pnCashierContainer.Size = new System.Drawing.Size(246, 280);
            this.pnCashierContainer.TabIndex = 0;
            // 
            // explorerBarCashier
            // 
            this.explorerBarCashier.Dock = System.Windows.Forms.DockStyle.Fill;
            this.explorerBarCashier.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            explorerBarItem1.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem1.Image")));
            explorerBarItem1.Key = "cmdCashier";
            explorerBarItem1.Text = "Màn hình Thu Ngân";
            explorerBarGroup1.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem1});
            explorerBarGroup1.Key = "Cashier";
            explorerBarGroup1.Text = "Thu Ngân";
            this.explorerBarCashier.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup1});
            this.explorerBarCashier.Location = new System.Drawing.Point(0, 0);
            this.explorerBarCashier.Name = "explorerBarCashier";
            this.explorerBarCashier.Size = new System.Drawing.Size(246, 280);
            this.explorerBarCashier.TabIndex = 2;
            this.explorerBarCashier.VisualStyleManager = this.vsMain;
            this.explorerBarCashier.ItemClick += new Janus.Windows.ExplorerBar.ItemEventHandler(this.explorerBarCashier_ItemClick);
            // 
            // pnTableAndRoom
            // 
            this.pnTableAndRoom.Image = ((System.Drawing.Image)(resources.GetObject("pnTableAndRoom.Image")));
            this.pnTableAndRoom.InnerContainer = this.pnClassRoomContainer;
            this.pnTableAndRoom.Location = new System.Drawing.Point(0, 0);
            this.pnTableAndRoom.Name = "pnTableAndRoom";
            this.pnTableAndRoom.Size = new System.Drawing.Size(248, 311);
            this.pnTableAndRoom.TabIndex = 4;
            this.pnTableAndRoom.Text = "Phòng bàn";
            // 
            // pnClassRoomContainer
            // 
            this.pnClassRoomContainer.Controls.Add(this.explorerBarTableAndRoom);
            this.pnClassRoomContainer.Location = new System.Drawing.Point(1, 31);
            this.pnClassRoomContainer.Name = "pnClassRoomContainer";
            this.pnClassRoomContainer.Size = new System.Drawing.Size(246, 280);
            this.pnClassRoomContainer.TabIndex = 0;
            // 
            // explorerBarTableAndRoom
            // 
            this.explorerBarTableAndRoom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.explorerBarTableAndRoom.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            explorerBarItem2.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem2.Image")));
            explorerBarItem2.Key = "cmdAdd";
            explorerBarItem2.Text = "Thêm mới";
            explorerBarItem3.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem3.Image")));
            explorerBarItem3.Key = "cmdManagement";
            explorerBarItem3.Text = "Theo dõi";
            explorerBarGroup2.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem2,
            explorerBarItem3});
            explorerBarGroup2.Key = "TableAndRoom";
            explorerBarGroup2.Text = "Phòng bàn";
            explorerBarItem4.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem4.Image")));
            explorerBarItem4.Key = "cmdArea";
            explorerBarItem4.Text = "Thêm mới";
            explorerBarItem5.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem5.Image")));
            explorerBarItem5.Key = "cmdAreaManagement";
            explorerBarItem5.Text = "Theo dõi";
            explorerBarGroup3.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem4,
            explorerBarItem5});
            explorerBarGroup3.Key = "Area";
            explorerBarGroup3.Text = "Khu Vực";
            this.explorerBarTableAndRoom.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup2,
            explorerBarGroup3});
            this.explorerBarTableAndRoom.Location = new System.Drawing.Point(0, 0);
            this.explorerBarTableAndRoom.Name = "explorerBarTableAndRoom";
            this.explorerBarTableAndRoom.Size = new System.Drawing.Size(246, 280);
            this.explorerBarTableAndRoom.TabIndex = 1;
            this.explorerBarTableAndRoom.VisualStyleManager = this.vsMain;
            this.explorerBarTableAndRoom.ItemClick += new Janus.Windows.ExplorerBar.ItemEventHandler(this.explorerBarTableAndRoom_ItemClick);
            // 
            // pnProducts
            // 
            this.pnProducts.Image = ((System.Drawing.Image)(resources.GetObject("pnProducts.Image")));
            this.pnProducts.InnerContainer = this.pnCategoryContainer;
            this.pnProducts.Location = new System.Drawing.Point(0, 0);
            this.pnProducts.Name = "pnProducts";
            this.pnProducts.Size = new System.Drawing.Size(248, 311);
            this.pnProducts.TabIndex = 4;
            this.pnProducts.Text = "Hàng hoá";
            // 
            // pnCategoryContainer
            // 
            this.pnCategoryContainer.Controls.Add(this.explorerBarProducts);
            this.pnCategoryContainer.Location = new System.Drawing.Point(1, 31);
            this.pnCategoryContainer.Name = "pnCategoryContainer";
            this.pnCategoryContainer.Size = new System.Drawing.Size(246, 280);
            this.pnCategoryContainer.TabIndex = 0;
            // 
            // explorerBarProducts
            // 
            this.explorerBarProducts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.explorerBarProducts.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            explorerBarItem6.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem6.Image")));
            explorerBarItem6.Key = "cmdAddCategory";
            explorerBarItem6.Text = "Thêm mới";
            explorerBarItem7.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem7.Image")));
            explorerBarItem7.Key = "cmdCategoryManagement";
            explorerBarItem7.Text = "Theo dõi";
            explorerBarGroup4.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem6,
            explorerBarItem7});
            explorerBarGroup4.Key = "Category";
            explorerBarGroup4.Text = "Nhóm hàng hoá";
            explorerBarItem8.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem8.Image")));
            explorerBarItem8.Key = "cmdAddProduct";
            explorerBarItem8.Text = "Thêm hàng hoá";
            explorerBarItem9.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem9.Image")));
            explorerBarItem9.Key = "cmdProductManagement";
            explorerBarItem9.Text = "Theo dõi";
            explorerBarGroup5.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem8,
            explorerBarItem9});
            explorerBarGroup5.Key = "Products";
            explorerBarGroup5.Text = "Hàng hoá";
            this.explorerBarProducts.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup4,
            explorerBarGroup5});
            this.explorerBarProducts.Location = new System.Drawing.Point(0, 0);
            this.explorerBarProducts.Name = "explorerBarProducts";
            this.explorerBarProducts.Size = new System.Drawing.Size(246, 280);
            this.explorerBarProducts.TabIndex = 2;
            this.explorerBarProducts.VisualStyleManager = this.vsMain;
            this.explorerBarProducts.ItemClick += new Janus.Windows.ExplorerBar.ItemEventHandler(this.explorerBarProducts_ItemClick);
            // 
            // pnCashFlow
            // 
            this.pnCashFlow.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnCashFlow.Image = ((System.Drawing.Image)(resources.GetObject("pnCashFlow.Image")));
            this.pnCashFlow.InnerContainer = this.pnCashFlowContainer;
            this.pnCashFlow.Location = new System.Drawing.Point(0, 0);
            this.pnCashFlow.Name = "pnCashFlow";
            this.pnCashFlow.Size = new System.Drawing.Size(248, 311);
            this.pnCashFlow.TabIndex = 4;
            this.pnCashFlow.Text = "Sổ quỹ";
            // 
            // pnCashFlowContainer
            // 
            this.pnCashFlowContainer.Controls.Add(this.explorerBarCashFlow);
            this.pnCashFlowContainer.Location = new System.Drawing.Point(1, 31);
            this.pnCashFlowContainer.Name = "pnCashFlowContainer";
            this.pnCashFlowContainer.Size = new System.Drawing.Size(246, 280);
            this.pnCashFlowContainer.TabIndex = 0;
            // 
            // explorerBarCashFlow
            // 
            this.explorerBarCashFlow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.explorerBarCashFlow.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            explorerBarItem10.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem10.Image")));
            explorerBarItem10.Key = "cmdCashFlowManagement";
            explorerBarItem10.Text = "Theo dõi";
            explorerBarGroup6.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem10});
            explorerBarGroup6.Key = "CashFlow";
            explorerBarGroup6.Text = "Sổ quỹ";
            explorerBarItem11.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem11.Image")));
            explorerBarItem11.Key = "cmdPayment";
            explorerBarItem11.Text = "Thêm mới";
            explorerBarItem12.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem12.Image")));
            explorerBarItem12.Key = "cmdPaymentType";
            explorerBarItem12.Text = "Thêm loại chi";
            explorerBarItem13.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem13.Image")));
            explorerBarItem13.Key = "cmdPaymentManagement";
            explorerBarItem13.Text = "Theo dõi";
            explorerBarGroup7.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem11,
            explorerBarItem12,
            explorerBarItem13});
            explorerBarGroup7.Key = "Payment";
            explorerBarGroup7.Text = "Phiếu chi";
            this.explorerBarCashFlow.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup6,
            explorerBarGroup7});
            this.explorerBarCashFlow.Location = new System.Drawing.Point(0, 0);
            this.explorerBarCashFlow.Name = "explorerBarCashFlow";
            this.explorerBarCashFlow.Size = new System.Drawing.Size(246, 280);
            this.explorerBarCashFlow.TabIndex = 2;
            this.explorerBarCashFlow.VisualStyleManager = this.vsMain;
            this.explorerBarCashFlow.ItemClick += new Janus.Windows.ExplorerBar.ItemEventHandler(this.explorerBarCashFlow_ItemClick);
            // 
            // cmdMain
            // 
            this.cmdMain.BottomRebar = this.BottomRebar1;
            this.cmdMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmdMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdBackupDatabase,
            this.cmdConfigConnection,
            this.cmdExit,
            this.cmdSQLQuery,
            this.cmdLog,
            this.cmdConfigSystem,
            this.cmdSystem,
            this.cmdLoginUser,
            this.cmdChangePassword,
            this.cmdUser});
            this.cmdMain.ContainerControl = this;
            this.cmdMain.Id = new System.Guid("a1e31820-d20c-43ff-bc01-0e38d9ce27e4");
            this.cmdMain.LeftRebar = this.LeftRebar1;
            this.cmdMain.RightRebar = this.RightRebar1;
            this.cmdMain.TopRebar = this.TopRebar1;
            this.cmdMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmdMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmdMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandBarType = Janus.Windows.UI.CommandBars.CommandBarType.Menu;
            this.uiCommandBar1.CommandManager = this.cmdMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSystem1});
            this.uiCommandBar1.CommandsStyle = Janus.Windows.UI.CommandBars.CommandStyle.TextImage;
            this.uiCommandBar1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiCommandBar1.Key = "cmdSystem";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(1055, 30);
            this.uiCommandBar1.Text = "Hệ thống";
            // 
            // cmdSystem1
            // 
            this.cmdSystem1.Key = "cmdSystem";
            this.cmdSystem1.Name = "cmdSystem1";
            // 
            // cmdBackupDatabase
            // 
            this.cmdBackupDatabase.Image = ((System.Drawing.Image)(resources.GetObject("cmdBackupDatabase.Image")));
            this.cmdBackupDatabase.Key = "cmdBackupDatabase";
            this.cmdBackupDatabase.Name = "cmdBackupDatabase";
            this.cmdBackupDatabase.Text = "Sao lưu dữ liệu";
            // 
            // cmdConfigConnection
            // 
            this.cmdConfigConnection.Image = ((System.Drawing.Image)(resources.GetObject("cmdConfigConnection.Image")));
            this.cmdConfigConnection.Key = "cmdConfigConnection";
            this.cmdConfigConnection.Name = "cmdConfigConnection";
            this.cmdConfigConnection.Text = "Cấu hình thông số kết nối";
            // 
            // cmdExit
            // 
            this.cmdExit.Image = ((System.Drawing.Image)(resources.GetObject("cmdExit.Image")));
            this.cmdExit.Key = "cmdExit";
            this.cmdExit.Name = "cmdExit";
            this.cmdExit.Text = "Thoát phần mềm";
            // 
            // cmdSQLQuery
            // 
            this.cmdSQLQuery.Image = ((System.Drawing.Image)(resources.GetObject("cmdSQLQuery.Image")));
            this.cmdSQLQuery.Key = "cmdSQLQuery";
            this.cmdSQLQuery.Name = "cmdSQLQuery";
            this.cmdSQLQuery.Text = "Thực hiện truy vấn SQL";
            // 
            // cmdLog
            // 
            this.cmdLog.Image = ((System.Drawing.Image)(resources.GetObject("cmdLog.Image")));
            this.cmdLog.Key = "cmdLog";
            this.cmdLog.Name = "cmdLog";
            this.cmdLog.Text = "Nhật ký chương trình";
            // 
            // cmdConfigSystem
            // 
            this.cmdConfigSystem.Image = ((System.Drawing.Image)(resources.GetObject("cmdConfigSystem.Image")));
            this.cmdConfigSystem.Key = "cmdConfigSystem";
            this.cmdConfigSystem.Name = "cmdConfigSystem";
            this.cmdConfigSystem.Text = "Cấu hình hệ thống";
            // 
            // cmdSystem
            // 
            this.cmdSystem.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdConfigSystem1,
            this.cmdConfigConnection1,
            this.cmdBackupDatabase1,
            this.cmdSQLQuery1,
            this.cmdLog1,
            this.cmdLoginUser1,
            this.cmdChangePassword1,
            this.cmdUser1,
            this.cmdExit1});
            this.cmdSystem.Image = ((System.Drawing.Image)(resources.GetObject("cmdSystem.Image")));
            this.cmdSystem.Key = "cmdSystem";
            this.cmdSystem.Name = "cmdSystem";
            this.cmdSystem.Text = "Hệ thống";
            // 
            // cmdConfigSystem1
            // 
            this.cmdConfigSystem1.Key = "cmdConfigSystem";
            this.cmdConfigSystem1.Name = "cmdConfigSystem1";
            // 
            // cmdConfigConnection1
            // 
            this.cmdConfigConnection1.Key = "cmdConfigConnection";
            this.cmdConfigConnection1.Name = "cmdConfigConnection1";
            // 
            // cmdBackupDatabase1
            // 
            this.cmdBackupDatabase1.Key = "cmdBackupDatabase";
            this.cmdBackupDatabase1.Name = "cmdBackupDatabase1";
            // 
            // cmdSQLQuery1
            // 
            this.cmdSQLQuery1.Key = "cmdSQLQuery";
            this.cmdSQLQuery1.Name = "cmdSQLQuery1";
            // 
            // cmdLog1
            // 
            this.cmdLog1.Key = "cmdLog";
            this.cmdLog1.Name = "cmdLog1";
            // 
            // cmdLoginUser1
            // 
            this.cmdLoginUser1.Key = "cmdLoginUser";
            this.cmdLoginUser1.Name = "cmdLoginUser1";
            // 
            // cmdChangePassword1
            // 
            this.cmdChangePassword1.Key = "cmdChangePassword";
            this.cmdChangePassword1.Name = "cmdChangePassword1";
            // 
            // cmdUser1
            // 
            this.cmdUser1.Key = "cmdUser";
            this.cmdUser1.Name = "cmdUser1";
            // 
            // cmdExit1
            // 
            this.cmdExit1.Key = "cmdExit";
            this.cmdExit1.Name = "cmdExit1";
            // 
            // cmdLoginUser
            // 
            this.cmdLoginUser.Image = ((System.Drawing.Image)(resources.GetObject("cmdLoginUser.Image")));
            this.cmdLoginUser.Key = "cmdLoginUser";
            this.cmdLoginUser.Name = "cmdLoginUser";
            this.cmdLoginUser.Text = "Đăng nhập người dùng khác";
            // 
            // cmdChangePassword
            // 
            this.cmdChangePassword.Image = ((System.Drawing.Image)(resources.GetObject("cmdChangePassword.Image")));
            this.cmdChangePassword.Key = "cmdChangePassword";
            this.cmdChangePassword.Name = "cmdChangePassword";
            this.cmdChangePassword.Text = "Đổi mật khẩu";
            // 
            // cmdUser
            // 
            this.cmdUser.Image = ((System.Drawing.Image)(resources.GetObject("cmdUser.Image")));
            this.cmdUser.Key = "cmdUser";
            this.cmdUser.Name = "cmdUser";
            this.cmdUser.Text = "Quản lý người dùng";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmdMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmdMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.cmdMain;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(1055, 30);
            // 
            // grbMain
            // 
            this.grbMain.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.grbMain.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.grbMain.BorderColor = System.Drawing.Color.Transparent;
            this.grbMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grbMain.ForeColor = System.Drawing.SystemColors.ControlText;
            this.grbMain.Location = new System.Drawing.Point(255, 33);
            this.grbMain.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.grbMain.Name = "grbMain";
            this.grbMain.Size = new System.Drawing.Size(797, 479);
            this.grbMain.TabIndex = 5;
            this.grbMain.Visible = false;
            this.grbMain.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // MainForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(1055, 515);
            this.Controls.Add(this.grbMain);
            this.Controls.Add(this.grPnSystem);
            this.Controls.Add(this.TopRebar1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Phần mềm quản lý Nhà hàng";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pnMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grPnSystem)).EndInit();
            this.grPnSystem.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnCashier)).EndInit();
            this.pnCashier.ResumeLayout(false);
            this.pnCashierContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.explorerBarCashier)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnTableAndRoom)).EndInit();
            this.pnTableAndRoom.ResumeLayout(false);
            this.pnClassRoomContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.explorerBarTableAndRoom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnProducts)).EndInit();
            this.pnProducts.ResumeLayout(false);
            this.pnCategoryContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.explorerBarProducts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnCashFlow)).EndInit();
            this.pnCashFlow.ResumeLayout(false);
            this.pnCashFlowContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.explorerBarCashFlow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.Common.VisualStyleManager vsMain;
        private Janus.Windows.UI.Dock.UIPanelManager pnMain;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UICommandManager cmdMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.Dock.UIPanelGroup grPnSystem;
        private Janus.Windows.UI.Dock.UIPanel pnTableAndRoom;
        private Janus.Windows.UI.Dock.UIPanelInnerContainer pnClassRoomContainer;
        private Janus.Windows.UI.Dock.UIPanel pnProducts;
        private Janus.Windows.UI.Dock.UIPanelInnerContainer pnCategoryContainer;
        private Janus.Windows.UI.Dock.UIPanel pnCashFlow;
        private Janus.Windows.UI.Dock.UIPanelInnerContainer pnCashFlowContainer;
        private Janus.Windows.ExplorerBar.ExplorerBar explorerBarTableAndRoom;
        private Janus.Windows.ExplorerBar.ExplorerBar explorerBarProducts;
        private Janus.Windows.EditControls.UIGroupBox grbMain;
        private Janus.Windows.ExplorerBar.ExplorerBar explorerBarCashFlow;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdBackupDatabase;
        private Janus.Windows.UI.CommandBars.UICommand cmdConfigConnection;
        private Janus.Windows.UI.CommandBars.UICommand cmdExit;
        private Janus.Windows.UI.CommandBars.UICommand cmdSQLQuery;
        private Janus.Windows.UI.CommandBars.UICommand cmdLog;
        private Janus.Windows.UI.CommandBars.UICommand cmdConfigSystem;
        private Janus.Windows.UI.Dock.UIPanel pnCashier;
        private Janus.Windows.UI.Dock.UIPanelInnerContainer pnCashierContainer;
        private Janus.Windows.ExplorerBar.ExplorerBar explorerBarCashier;
        private Janus.Windows.UI.CommandBars.UICommand cmdSystem;
        private Janus.Windows.UI.CommandBars.UICommand cmdConfigSystem1;
        private Janus.Windows.UI.CommandBars.UICommand cmdConfigConnection1;
        private Janus.Windows.UI.CommandBars.UICommand cmdBackupDatabase1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSQLQuery1;
        private Janus.Windows.UI.CommandBars.UICommand cmdLog1;
        private Janus.Windows.UI.CommandBars.UICommand cmdExit1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSystem1;
        private Janus.Windows.UI.CommandBars.UICommand cmdLoginUser;
        private Janus.Windows.UI.CommandBars.UICommand cmdChangePassword;
        private Janus.Windows.UI.CommandBars.UICommand cmdLoginUser1;
        private Janus.Windows.UI.CommandBars.UICommand cmdChangePassword1;
        private Janus.Windows.UI.CommandBars.UICommand cmdUser1;
        private Janus.Windows.UI.CommandBars.UICommand cmdUser;
    }
}