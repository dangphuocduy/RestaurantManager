﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace RestaurantManager
{
	public partial class PhongBan : ICloneable
	{
		#region Properties.
		
		public long Id { set; get; }
		public string TenPhongBan { set; get; }
		public long KhuVucId { set; get; }
		public int STT { set; get; }
		public int SoGhe { set; get; }
		public string GhiChu { set; get; }
		public DateTime ThoiGianTao { set; get; }
		public string KhachHang { set; get; }
        public List<PhongBan_HangHoa> HangHoaCollection { set; get; }
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<PhongBan> ConvertToCollection(IDataReader reader)
		{
			List<PhongBan> collection = new List<PhongBan>();
			while (reader.Read())
			{
				PhongBan entity = new PhongBan();
				if (!reader.IsDBNull(reader.GetOrdinal("Id"))) entity.Id = reader.GetInt64(reader.GetOrdinal("Id"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenPhongBan"))) entity.TenPhongBan = reader.GetString(reader.GetOrdinal("TenPhongBan"));
				if (!reader.IsDBNull(reader.GetOrdinal("KhuVucId"))) entity.KhuVucId = reader.GetInt64(reader.GetOrdinal("KhuVucId"));
				if (!reader.IsDBNull(reader.GetOrdinal("STT"))) entity.STT = reader.GetInt32(reader.GetOrdinal("STT"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoGhe"))) entity.SoGhe = reader.GetInt32(reader.GetOrdinal("SoGhe"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThoiGianTao"))) entity.ThoiGianTao = reader.GetDateTime(reader.GetOrdinal("ThoiGianTao"));
				if (!reader.IsDBNull(reader.GetOrdinal("KhachHang"))) entity.KhachHang = reader.GetString(reader.GetOrdinal("KhachHang"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<PhongBan> collection, long id)
        {
            foreach (PhongBan item in collection)
            {
                if (item.Id == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_PhongBan VALUES(@TenPhongBan, @KhuVucId, @STT, @SoGhe, @GhiChu, @ThoiGianTao, @KhachHang)";
            string update = "UPDATE t_PhongBan SET TenPhongBan = @TenPhongBan, KhuVucId = @KhuVucId, STT = @STT, SoGhe = @SoGhe, GhiChu = @GhiChu, ThoiGianTao = @ThoiGianTao, KhachHang = @KhachHang WHERE Id = @Id";
            string delete = "DELETE FROM t_PhongBan WHERE Id = @Id";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@Id", SqlDbType.BigInt, "Id", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenPhongBan", SqlDbType.NVarChar, "TenPhongBan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KhuVucId", SqlDbType.BigInt, "KhuVucId", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@STT", SqlDbType.Int, "STT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoGhe", SqlDbType.Int, "SoGhe", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThoiGianTao", SqlDbType.DateTime, "ThoiGianTao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KhachHang", SqlDbType.NVarChar, "KhachHang", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@Id", SqlDbType.BigInt, "Id", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenPhongBan", SqlDbType.NVarChar, "TenPhongBan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KhuVucId", SqlDbType.BigInt, "KhuVucId", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@STT", SqlDbType.Int, "STT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoGhe", SqlDbType.Int, "SoGhe", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThoiGianTao", SqlDbType.DateTime, "ThoiGianTao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KhachHang", SqlDbType.NVarChar, "KhachHang", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@Id", SqlDbType.BigInt, "Id", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_PhongBan VALUES(@TenPhongBan, @KhuVucId, @STT, @SoGhe, @GhiChu, @ThoiGianTao, @KhachHang)";
            string update = "UPDATE t_PhongBan SET TenPhongBan = @TenPhongBan, KhuVucId = @KhuVucId, STT = @STT, SoGhe = @SoGhe, GhiChu = @GhiChu, ThoiGianTao = @ThoiGianTao, KhachHang = @KhachHang WHERE Id = @Id";
            string delete = "DELETE FROM t_PhongBan WHERE Id = @Id";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@Id", SqlDbType.BigInt, "Id", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenPhongBan", SqlDbType.NVarChar, "TenPhongBan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KhuVucId", SqlDbType.BigInt, "KhuVucId", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@STT", SqlDbType.Int, "STT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoGhe", SqlDbType.Int, "SoGhe", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThoiGianTao", SqlDbType.DateTime, "ThoiGianTao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KhachHang", SqlDbType.NVarChar, "KhachHang", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@Id", SqlDbType.BigInt, "Id", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenPhongBan", SqlDbType.NVarChar, "TenPhongBan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KhuVucId", SqlDbType.BigInt, "KhuVucId", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@STT", SqlDbType.Int, "STT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoGhe", SqlDbType.Int, "SoGhe", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThoiGianTao", SqlDbType.DateTime, "ThoiGianTao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KhachHang", SqlDbType.NVarChar, "KhachHang", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@Id", SqlDbType.BigInt, "Id", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static PhongBan Load(long id)
		{
			const string spName = "[dbo].[p_PhongBan_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@Id", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<PhongBan> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<PhongBan> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<PhongBan> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<PhongBan> SelectCollectionBy_KhuVucId(long khuVucId)
		{
            IDataReader reader = SelectReaderBy_KhuVucId(khuVucId);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_KhuVucId(long khuVucId)
		{
			const string spName = "[dbo].[p_PhongBan_SelectBy_KhuVucId]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@KhuVucId", SqlDbType.BigInt, khuVucId);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_PhongBan_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_PhongBan_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_PhongBan_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_PhongBan_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_KhuVucId(long khuVucId)
		{
			const string spName = "p_PhongBan_SelectBy_KhuVucId";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@KhuVucId", SqlDbType.BigInt, khuVucId);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertPhongBan(string tenPhongBan, long khuVucId, int sTT, int soGhe, string ghiChu, DateTime thoiGianTao, string khachHang)
		{
			PhongBan entity = new PhongBan();	
			entity.TenPhongBan = tenPhongBan;
			entity.KhuVucId = khuVucId;
			entity.STT = sTT;
			entity.SoGhe = soGhe;
			entity.GhiChu = ghiChu;
			entity.ThoiGianTao = thoiGianTao;
			entity.KhachHang = khachHang;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_PhongBan_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@Id", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@TenPhongBan", SqlDbType.NVarChar, TenPhongBan);
			db.AddInParameter(dbCommand, "@KhuVucId", SqlDbType.BigInt, KhuVucId);
			db.AddInParameter(dbCommand, "@STT", SqlDbType.Int, STT);
			db.AddInParameter(dbCommand, "@SoGhe", SqlDbType.Int, SoGhe);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@ThoiGianTao", SqlDbType.DateTime, ThoiGianTao.Year <= 1753 ? DBNull.Value : (object) ThoiGianTao);
			db.AddInParameter(dbCommand, "@KhachHang", SqlDbType.NVarChar, KhachHang);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				Id = (long) db.GetParameterValue(dbCommand, "@Id");
				return Id;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				Id = (long) db.GetParameterValue(dbCommand, "@Id");
				return Id;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<PhongBan> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (PhongBan item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdatePhongBan(long id, string tenPhongBan, long khuVucId, int sTT, int soGhe, string ghiChu, DateTime thoiGianTao, string khachHang)
		{
			PhongBan entity = new PhongBan();			
			entity.Id = id;
			entity.TenPhongBan = tenPhongBan;
			entity.KhuVucId = khuVucId;
			entity.STT = sTT;
			entity.SoGhe = soGhe;
			entity.GhiChu = ghiChu;
			entity.ThoiGianTao = thoiGianTao;
			entity.KhachHang = khachHang;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_PhongBan_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@Id", SqlDbType.BigInt, Id);
			db.AddInParameter(dbCommand, "@TenPhongBan", SqlDbType.NVarChar, TenPhongBan);
			db.AddInParameter(dbCommand, "@KhuVucId", SqlDbType.BigInt, KhuVucId);
			db.AddInParameter(dbCommand, "@STT", SqlDbType.Int, STT);
			db.AddInParameter(dbCommand, "@SoGhe", SqlDbType.Int, SoGhe);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@ThoiGianTao", SqlDbType.DateTime, ThoiGianTao.Year <= 1753 ? DBNull.Value : (object) ThoiGianTao);
			db.AddInParameter(dbCommand, "@KhachHang", SqlDbType.NVarChar, KhachHang);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<PhongBan> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (PhongBan item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdatePhongBan(long id, string tenPhongBan, long khuVucId, int sTT, int soGhe, string ghiChu, DateTime thoiGianTao, string khachHang)
		{
			PhongBan entity = new PhongBan();			
			entity.Id = id;
			entity.TenPhongBan = tenPhongBan;
			entity.KhuVucId = khuVucId;
			entity.STT = sTT;
			entity.SoGhe = soGhe;
			entity.GhiChu = ghiChu;
			entity.ThoiGianTao = thoiGianTao;
			entity.KhachHang = khachHang;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_PhongBan_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@Id", SqlDbType.BigInt, Id);
			db.AddInParameter(dbCommand, "@TenPhongBan", SqlDbType.NVarChar, TenPhongBan);
			db.AddInParameter(dbCommand, "@KhuVucId", SqlDbType.BigInt, KhuVucId);
			db.AddInParameter(dbCommand, "@STT", SqlDbType.Int, STT);
			db.AddInParameter(dbCommand, "@SoGhe", SqlDbType.Int, SoGhe);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@ThoiGianTao", SqlDbType.DateTime, ThoiGianTao.Year <= 1753 ? DBNull.Value : (object) ThoiGianTao);
			db.AddInParameter(dbCommand, "@KhachHang", SqlDbType.NVarChar, KhachHang);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<PhongBan> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (PhongBan item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeletePhongBan(long id)
		{
			PhongBan entity = new PhongBan();
			entity.Id = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_PhongBan_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@Id", SqlDbType.BigInt, Id);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_KhuVucId(long khuVucId)
		{
			const string spName = "[dbo].[p_PhongBan_DeleteBy_KhuVucId]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@KhuVucId", SqlDbType.BigInt, khuVucId);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_PhongBan_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<PhongBan> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (PhongBan item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion

        public void InsertUpdateFull()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.Id == 0)
                    {
                        this.Id = this.Insert(transaction);
                    }
                    else
                    {
                        this.Update(transaction);
                    }

                    foreach (PhongBan_HangHoa item in HangHoaCollection)
                    {
                        item.PhongBanId = this.Id;
                        if (item.Id == 0)
                        {
                            item.Insert(transaction);
                        }
                        else
                        {
                            item.Update(transaction);
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

        }

        public void DeleteFull()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    foreach (PhongBan_HangHoa item in HangHoaCollection)
                    {
                        item.Delete();
                    }
                    this.Delete();
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

        }
	}	
}